<?php

declare(strict_types=1);

namespace Drupal\lms\Hook;

use Drupal\Core\Hook\Attribute\Hook;

/**
 * Contains all hooks that don't need any services injected.
 */
final class OtherHooks {

  #[Hook('theme')]
  public function theme(): array {
    return [
      'lms_lp_step_activity' => [
        'render element' => 'elements',
      ],
      'lms_lp_step_module' => [
        'render element' => 'elements',
      ],
      'lms_lp_step_module_activity' => [
        'render element' => 'elements',
      ],
      'lms_course_action_info' => [
        'variables' => [
          'class' => NULL,
          'info_text' => NULL,
        ],
      ],
      'lms_start_link' => [
        'variables' => [
          'link' => NULL,
          'action_info' => NULL,
        ],
      ],
      'lms_lesson_timer' => [
        'variables' => [
          'close_time' => 0,
        ],
      ],
    ];
  }

  #[Hook('element_info_alter')]
  public function elementInfoAlter(array &$types): void {
    // Attach our extra CSS for toolbar icons.
    if (\array_key_exists('toolbar', $types)) {
      $types['toolbar']['#attached']['library'][] = 'lms/toolbar';
    }
  }

}
