<?php

declare(strict_types=1);

namespace Drupal\lms\Hook;

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Access\AccessResultInterface;
use Drupal\Core\Cache\CacheTagsInvalidatorInterface;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\Core\Field\FieldStorageDefinitionInterface;
use Drupal\Core\Hook\Attribute\Hook;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\group\Access\GroupAccessResult;
use Drupal\group\Entity\GroupInterface;
use Drupal\group\Entity\GroupRelationshipInterface;
use Drupal\entity\BundleFieldDefinition;
use Drupal\lms\Entity\Bundle\Course;
use Drupal\lms\Entity\CourseStatus;
use Drupal\lms\Entity\LessonStatus;
use Drupal\lms\Plugin\Field\FieldType\StartLinkFieldItemList;
use Drupal\lms\TrainingManager;
use Symfony\Component\DependencyInjection\Attribute\Autowire;
use Symfony\Component\DependencyInjection\Attribute\AutowireCallable;

/**
 * Centralized entity hooks logic.
 */
final class EntityHooks {

  use StringTranslationTrait;

  /**
   * The constructor.
   */
  public function __construct(
    #[Autowire(service: 'entity_type.manager', lazy: TRUE)]
    protected readonly EntityTypeManagerInterface $entityTypeManager,
    #[AutowireCallable(service: 'queue', method: 'get', lazy: TRUE)]
    private \Closure $getQueue,
    #[Autowire(service: 'cache_tags.invalidator', lazy: TRUE)]
    protected readonly CacheTagsInvalidatorInterface $cacheTagsInvalidator,
  ) {}

  #[Hook('entity_base_field_info')]
  public function entityBaseFieldInfo(EntityTypeInterface $entity_type): array {
    $fields = [];

    // Add required fields globally to group entity type to avoid
    // reports of mismatched definitions and missing storage.
    // Unfortunately definitions in the bundle class are not enough for the
    // time being and using hook_entity_bundle_field_info() and
    // hook_entity_field_storage_info() that seems a bit cleaner completely
    // overrides definitions in the bundle class, making them obsolete.
    // @todo Wait for times when this gets better support in core.
    // @see Drupal\lms\Entity\Bundle\Course::bundleFieldDefinitions().
    if ($entity_type->id() === 'group') {
      $fields['lessons'] = BaseFieldDefinition::create('lms_reference')
        ->setSetting('target_type', 'lms_lesson')
        ->setRevisionable(TRUE)
        ->setTranslatable(FALSE)
        ->setCardinality(FieldStorageDefinitionInterface::CARDINALITY_UNLIMITED);
      $fields['revisit_mode'] = BaseFieldDefinition::create('boolean')
        ->setRevisionable(TRUE)
        ->setTranslatable(FALSE);
      $fields['free_navigation'] = BaseFieldDefinition::create('boolean')
        ->setRevisionable(TRUE)
        ->setTranslatable(FALSE);
    }

    return $fields;
  }

  #[Hook('entity_bundle_field_info')]
  public function entityBundleFieldInfo(EntityTypeInterface $entity_type, string $bundle): array {
    $fields = [];

    if ($entity_type->id() === 'group' && $bundle === 'lms_course') {
      $fields['start_link'] = BundleFieldDefinition::create('string')
        ->setTargetEntityTypeId('group')
        ->setTargetBundle('lms_course')
        ->setName('start_link')
        ->setLabel($this->t('Start link'))
        ->setTranslatable(FALSE)
        ->setCardinality(1)
        ->setComputed(TRUE)
        ->setDisplayConfigurable('form', FALSE)
        ->setDisplayConfigurable('view', TRUE)
        ->setDisplayOptions('view', [
          'type' => 'string',
          'label' => 'hidden',
          'weight' => 10,
          'region' => 'content',
        ])
        ->setClass(StartLinkFieldItemList::class);
    }

    return $fields;
  }

  #[Hook('entity_bundle_info')]
  public function entityBundleInfo(): array {
    $info = [];
    $info['lms_lesson']['lesson']['label'] = $this->t('Lesson (default)');
    return $info;
  }

  #[Hook('entity_bundle_info_alter')]
  public function entityBundleInfoAlter(array &$bundles): void {
    if (\array_key_exists('group', $bundles) && \array_key_exists('lms_course', $bundles['group'])) {
      $bundles['group']['lms_course']['class'] = Course::class;
    }
  }

  #[Hook('group_insert')]
  public function groupInsert(GroupInterface $group): void {
    if (!$group instanceof Course) {
      return;
    }

    // Add a default class on learning path creation.
    /**
     * @var \Drupal\group\Entity\GroupInterface
     */
    $class = $this->entityTypeManager->getStorage('group')->create([
      'type' => 'lms_class',
      'label' => self::generateDefaultClassName($group),
      'uid' => $group->getOwnerId(),
    ]);
    $class->save();

    // Always add the creator membership.
    $group_type = $class->getGroupType();
    if ($group_type->creatorMustCompleteMembership()) {
      $values = ['group_roles' => $group_type->getCreatorRoleIds()];
      $class->addMember($class->getOwner(), $values);
    }

    $group->addRelationship($class, 'lms_classes');
  }

  #[Hook('group_delete')]
  public function groupDelete(GroupInterface $group): void {
    if ($group instanceof Course) {
      CourseStatus::actOnCourseDeletion($group->id());
    }
  }

  #[Hook('lms_course_status_delete')]
  public function lmsCourseStatusDelete(CourseStatus $course_status): void {
    // Queue lesson statuses that reference this Course status for deletion.
    $lesson_status_ids = $this->entityTypeManager->getStorage('lms_lesson_status')->getQuery()
      ->accessCheck(FALSE)
      ->condition('course_status', $course_status->id())
      ->execute();
    $queue = ($this->getQueue)('lms_delete_entities', TRUE);
    foreach (array_chunk($lesson_status_ids, 10) as $ids) {
      $queue->createItem(['entity_type' => 'lms_lesson_status', 'ids' => $ids]);
    }
    // @todo Not sure that cache invalidation should occur here or on deletion.
    $this->cacheTagsInvalidator->invalidateTags([TrainingManager::trainingStatusTag($course_status->getCourseId(), $course_status->getUserId())]);
  }

  #[Hook('lms_course_status_presave')]
  public function lmsCourseStatusPresave(CourseStatus $course_status): void {
    $this->cacheTagsInvalidator->invalidateTags([TrainingManager::trainingStatusTag($course_status->getCourseId(), $course_status->getUserId())]);
  }

  #[Hook('lms_lesson_status_delete')]
  public function lmsLessonStatusDelete(LessonStatus $lesson_status): void {
    // Queue answers that reference this lesson status for deletion.
    $answer_ids = $this->entityTypeManager->getStorage('lms_answer')->getQuery()
      ->accessCheck(FALSE)
      ->condition('lesson_status', $lesson_status->id())
      ->execute();
    $queue = ($this->getQueue)('lms_delete_entities', TRUE);
    foreach (array_chunk($answer_ids, 10) as $ids) {
      $queue->createItem(['entity_type' => 'lms_answer', 'ids' => $ids]);
    }
  }

  #[Hook('group_access')]
  public function groupAccess(GroupInterface $group, string $operation, AccountInterface $account): AccessResultInterface {
    if ($group instanceof Course) {
      if ($operation === 'take') {
        return $group->takeAccess($account);
      }
      if ($operation === 'results') {
        $access = $group->takeAccess($account);
        if (!$access->isAllowed()) {
          return GroupAccessResult::allowedIfHasGroupPermission($group, $account, 'edit group');
        }
        return $access;
      }
      if ($operation === 'view') {
        // If the user can take the course, allow view access as well.
        $access = $group->takeAccess($account);
        if ($access->isAllowed()) {
          return $access;
        }
      }
    }

    return AccessResult::neutral();
  }

  #[Hook('group_relationship_presave')]
  #[Hook('lms_group_relationship_delete')]
  public function invalidateRelationshipTags(GroupRelationshipInterface $group_relationship): void {
    // Invalidate course classes tag.
    if ($group_relationship->getPluginId() === 'lms_classes') {
      $parent_group = $group_relationship->getGroup();
      if (!$parent_group instanceof Course) {
        return;
      }
      $this->cacheTagsInvalidator->invalidateTags(['group.classes:' . $parent_group->id()]);
    }

    // Invalidate course membership request tag.
    elseif ($group_relationship->getPluginId() === 'group_membership_request') {
      $this->cacheTagsInvalidator->invalidateTags(['lms_course_membership:' . $group_relationship->getGroupId() . ':' . $group_relationship->getEntityId()]);
    }

    // Invalidate course take access tags.
    if ($group_relationship->getPluginId() === 'group_membership') {
      if (\in_array($group_relationship->getGroupTypeId(), [
        'lms_class',
        'lms_course',
      ], TRUE)) {
        // @todo Make this Course-specific (if a class, get parent group ID).
        $this->cacheTagsInvalidator->invalidateTags(['lms_memberships:' . $group_relationship->getEntityId()]);
      }
    }
  }

  /**
   * Generate default class name.
   */
  public static function generateDefaultClassName(Course $course): string {
    // Generate a class name based on the learning path. Using the learning
    // path name could make group listings hard to scan. Using the learning
    // path ID could create confusion with the class's own numeric ID. To
    // avoid this and also avoid hash collisions, add 100 to the learning
    // path ID then base36 encode and upper-case it. This gives us a
    // human-readable short hash which is superficially similar to the
    // kind of class names that are often used in schools (1A, DF1, etc.).
    return (string) \t('Class @class_name', [
      '@class_name' => \strtoupper(\base_convert((string) ($course->id() + 100), 10, 36)),
    ]);
  }

}
