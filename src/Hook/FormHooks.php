<?php

declare(strict_types=1);

namespace Drupal\lms\Hook;

use Drupal\Core\Entity\EntityFormInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Hook\Attribute\Hook;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\Core\Url;
use Drupal\lms\Entity\Bundle\Course;
use Drupal\lms\Form\DataIntegrityWarningTrait;
use Symfony\Component\DependencyInjection\Attribute\AutowireCallable;

/**
 * Simple form alter hooks.
 */
final class FormHooks {

  use StringTranslationTrait;
  use DataIntegrityWarningTrait;

  public function __construct(
    #[AutowireCallable(service: 'lms.data_integrity_checker', method: 'checkCourseProgress', lazy: TRUE)]
    private \Closure $checkCourseProgress,
  ) {}

  #[Hook('form_views_exposed_form_alter')]
  public function formViewsExposedFormAlter(array &$form, FormStateInterface $form_state): void {
    // We don't want filter buttons at the bottom of modals.
    if ($form['#action'] === Url::fromRoute('lms.modal_subform_endpoint')->toString()) {
      $form['actions']['#type'] = 'container';
      $form['actions']['#attributes'] = ['class' => 'modal-form-actions'];
    }
  }

  #[Hook('form_group_form_alter')]
  public function groupFormAlter(array &$form, FormStateInterface $form_state): void {
    $form_object = $form_state->getFormObject();
    \assert($form_object instanceof EntityFormInterface);
    $group = $form_object->getEntity();
    if (!$group instanceof Course) {
      return;
    }
    $in_progress = ($this->checkCourseProgress)($group);
    if ($in_progress) {
      $affected_fields = [
        'lessons',
      ];
      foreach ($affected_fields as $field) {
        $this->addDataWarning($form[$field], $this->t('This course has already been started by certain students, changing the field below may cause their current progress to become inconsistent with the new value.'));
      }
    }
  }

}
