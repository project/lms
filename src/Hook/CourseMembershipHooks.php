<?php

declare(strict_types=1);

namespace Drupal\lms\Hook;

use Drupal\Core\Entity\ContentEntityFormInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Hook\Attribute\Hook;
use Drupal\Core\Link;
use Drupal\group\Entity\Form\GroupRelationshipDeleteForm;
use Drupal\group\Entity\GroupRelationshipInterface;
use Drupal\lms\Entity\Bundle\Course;

/**
 * Membership form alter hooks logic.
 */
final class CourseMembershipHooks extends CourseMembershipHooksBase {

  #[Hook('form_group_relationship_form_alter')]
  #[Hook('form_group_relationship_confirm_form_alter')]
  public function groupRelationshipFormAlter(array &$form, FormStateInterface $form_state): void {
    $form_object = $form_state->getFormObject();
    if (
      !$form_object instanceof ContentEntityFormInterface ||
      $form_object instanceof GroupRelationshipDeleteForm
    ) {
      return;
    }

    $relationship = $form_object->getEntity();
    \assert($relationship instanceof GroupRelationshipInterface);
    $relationship_plugin_id = $relationship->getPluginId();
    if ($relationship_plugin_id !== 'group_membership') {
      return;
    }

    $course = $relationship->getGroup();
    if (!$course instanceof Course) {
      return;
    }

    $form_state->set('redirect_group_id', $course->id());
    $form['actions']['submit']['#submit'][] = [
      self::class,
      'redirectToCourse',
    ];
    $form['actions']['cancel'] = Link::createFromRoute($this->t('Cancel'),
      'entity.group.canonical',
      ['group' => $course->id()],
      ['attributes' => ['class' => 'button']]
    )->toRenderable();

    $self_join = $relationship->getOwnerId() === $relationship->getEntityId();
    if ($self_join) {
      $form['actions']['submit']['#value'] = $this->t('Join training');
    }
    $form['#validate'][] = [
      $this,
      'membershipFormValidate',
    ];

    $this->addClassSelector($form, $course, $self_join);

    $form['actions']['submit']['#submit'][] = [
      $this,
      'addedToClassMessage',
    ];
  }

}
