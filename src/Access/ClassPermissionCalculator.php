<?php

declare(strict_types=1);

namespace Drupal\lms\Access;

use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\flexible_permissions\CalculatedPermissionsItem;
use Drupal\flexible_permissions\PermissionCalculatorBase;
use Drupal\flexible_permissions\RefinableCalculatedPermissionsInterface;
use Drupal\group\Entity\GroupRelationshipInterface;
use Drupal\group\PermissionScopeInterface;
use Drupal\lms\Entity\Bundle\Course;

/**
 * Calculates inherited group permissions for an account.
 */
final class ClassPermissionCalculator extends PermissionCalculatorBase {

  /**
   * The constructor.
   */
  public function __construct(
    private readonly EntityTypeManagerInterface $entityTypeManager,
  ) {}

  /**
   * {@inheritdoc}
   */
  public function calculatePermissions(AccountInterface $account, $scope) {
    $calculated_permissions = parent::calculatePermissions($account, $scope);
    \assert($calculated_permissions instanceof RefinableCalculatedPermissionsInterface);

    if ($scope !== PermissionScopeInterface::INDIVIDUAL_ID) {
      return $calculated_permissions;
    }

    // The inherited permissions need to be recalculated whenever the user is
    // added to or removed from a group.
    $calculated_permissions->addCacheTags(['group_relationship_list:plugin:group_membership:entity:' . $account->id()]);

    // 1. Course members should always be able to see classes
    //    and their members.
    $memberships = $this->entityTypeManager->getStorage('group_relationship')->loadByProperties([
      'plugin_id' => 'group_membership',
      'entity_id' => $account->id(),
      'group_type' => 'lms_course',
    ]);
    $class_ids = [];
    foreach ($memberships as $membership) {
      $calculated_permissions->addCacheableDependency($membership);

      \assert($membership instanceof GroupRelationshipInterface);
      $course = $membership->getGroup();
      \assert($course instanceof Course);
      foreach ($course->getClasses() as $class) {
        $class_id = $class->id();
        if (\array_key_exists($class_id, $class_ids)) {
          continue;
        }
        $class_ids[$class_id] = TRUE;

        $calculated_permissions->addItem(new CalculatedPermissionsItem(
          $scope,
          $class_id,
          [
            'administer members',
            'view group',
          ],
          FALSE,
        ));
      }
    }

    // 2. Class members should always have view published course permission
    //    on the parent course group.
    $memberships = $this->entityTypeManager->getStorage('group_relationship')->loadByProperties([
      'plugin_id' => 'group_membership',
      'entity_id' => $account->id(),
      'group_type' => 'lms_class',
    ]);
    $courses = [];

    foreach ($memberships as $membership) {
      \assert($membership instanceof GroupRelationshipInterface);
      $course_relationships = $this->entityTypeManager->getStorage('group_relationship')->loadByProperties([
        'plugin_id' => 'lms_classes',
        'entity_id' => $membership->getGroupId(),
        'group_type' => 'lms_course',
      ]);

      $has_courses = FALSE;
      foreach ($course_relationships as $relationship) {
        \assert($relationship instanceof GroupRelationshipInterface);
        $course = $relationship->getGroup();
        if (!$course instanceof Course) {
          continue;
        }
        $has_courses = TRUE;
        if (!\array_key_exists($course->id(), $courses)) {
          $courses[$course->id()] = $course;
          $calculated_permissions->addCacheableDependency($course);
        }
      }

      if ($has_courses) {
        $calculated_permissions->addCacheableDependency($membership);
      }

      foreach ($courses as $course) {
        $calculated_permissions->addItem(new CalculatedPermissionsItem(
          $scope,
          $course->id(),
          ['view group'],
          FALSE
        ));
      }
    }

    return $calculated_permissions;
  }

  /**
   * {@inheritdoc}
   */
  public function getPersistentCacheContexts($scope) {
    if ($scope === PermissionScopeInterface::INDIVIDUAL_ID) {
      return ['user'];
    }
    return [];
  }

}
