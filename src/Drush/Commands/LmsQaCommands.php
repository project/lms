<?php

declare(strict_types=1);

namespace Drupal\lms\Drush\Commands;

use Consolidation\AnnotatedCommand\CommandData;
use Consolidation\AnnotatedCommand\Hooks\HookManager;
use Consolidation\SiteAlias\SiteAliasManagerAwareInterface;
use Consolidation\SiteAlias\SiteAliasManagerAwareTrait;
use Drupal\Component\Serialization\Yaml;
use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Extension\ModuleExtensionList;
use Drupal\Core\Password\PasswordGeneratorInterface;
use Drupal\lms\ActivityAnswerManager;
use Drupal\lms\Config\PluginConfigInstaller;
use Drupal\lms\Entity\Bundle\Course;
use Drupal\lms\Entity\LessonInterface;
use Drupal\lms\TrainingManager;
use Drupal\user\UserInterface;
use Drush\Attributes as CLI;
use Drush\Commands\AutowireTrait;
use Drush\Commands\DrushCommands;
use Drush\Commands\core\QueueCommands;
use Symfony\Contracts\EventDispatcher\EventDispatcherInterface;

/**
 * Provides document fetching commands.
 */
final class LmsQaCommands extends DrushCommands implements SiteAliasManagerAwareInterface {

  use AutowireTrait;
  use SiteAliasManagerAwareTrait;

  public function __construct(
    private readonly ActivityAnswerManager $activityAnswerManager,
    private readonly EntityTypeManagerInterface $entityTypeManager,
    private readonly PluginConfigInstaller $pluginConfigInstaller,
    private readonly PasswordGeneratorInterface $passwordGenerator,
    private readonly TrainingManager $trainingManager,
    private readonly ModuleExtensionList $moduleExtensionList,
    private readonly EventDispatcherInterface $eventDispatcher,
  ) {
    parent::__construct();
  }

  /**
   * Create test content.
   */
  #[CLI\Command(name: 'lms:create-test-content', aliases: ['lms-ctt'])]
  #[CLI\Option(name: 'module', description: 'The module to be tested.')]
  #[CLI\Option(name: 'simple-passwords', description: 'Should we use "123456" for all passwords or generate them?')]
  #[CLI\Usage(name: 'drush lms-ctt', description: 'Create content used in tests.')]
  public function createTestContent(
    array $options = [
      'module' => 'lms_answer_plugins',
      'simple-passwords' => FALSE,
    ],
  ): void {

    // Install activity types.
    $storage = $this->entityTypeManager->getStorage('lms_activity_type');
    $definitions = $this->activityAnswerManager->getDefinitions();
    foreach ($definitions as $definition) {
      if (!\in_array($definition['provider'], [
        'lms',
        $options['module'],
      ], TRUE)) {
        continue;
      }
      $activity_type = $storage->load($definition['id']);
      // Could be already installed from existing config.
      if ($activity_type !== NULL) {
        continue;
      }
      $activity_type = $storage->create([
        'id' => $definition['id'],
        'name' => $definition['name'],
        'description' => '',
        'pluginId' => $definition['id'],
      ]);
      $activity_type->save();
      $this->pluginConfigInstaller->install($definition, $activity_type->id());
    }

    // Create content.
    if ($options['module'] === 'lms_answer_plugins') {
      $source_directory = $this->moduleExtensionList->getPath('lms') . '/tests/data/';
    }
    else {
      $source_directory = $this->moduleExtensionList->getPath($options['module']) . '/tests/data/';
    }

    $admin_uuid = NULL;
    foreach ([
      'users' => 'user',
      'activities' => 'lms_activity',
      'lessons' => 'lms_lesson',
      'courses' => 'group',
    ] as $source => $entity_type_id) {
      $file_path = $source_directory . $source . '.yml';
      if (!\file_exists($file_path)) {
        continue;
      }
      $data = Yaml::decode(\file_get_contents($file_path));
      if ($entity_type_id === 'user') {
        $admin_uuid = $data['admin']['uuid'];
      }
      $storage = $this->entityTypeManager->getStorage($entity_type_id);
      foreach ($data as $entity_data) {
        $entity = $storage->create($this->convertData($entity_data, $entity_type_id, $options));
        $entity->save();
      }
    }

    $event = new QaContentEvent($options['module']);
    $this->eventDispatcher->dispatch($event, $event::NAME);

    // Add LMS Admin role for user 1 to ensure all group permissions
    // are granted.
    $role = $this->entityTypeManager->getStorage('user_role')->load('lms_admin');
    if ($role !== NULL) {
      $superuser = $this->entityTypeManager->getStorage('user')->load('1');
      $superuser->addRole('lms_admin')->save();
    }

    if ($admin_uuid === NULL) {
      return;
    }

    $process = $this->processManager()->drush($this->siteAliasManager()->getSelf(), 'user:login', [], ['uid' => $this->getValueFromUuid('user', $admin_uuid)]);
    $process->mustRun();

    $this->logger->success(\dt('QA content has been created. LMS Admin login link: @link', [
      '@link' => $process->getOutput(),
    ]));
  }

  /**
   * Test data conversion helper.
   */
  private function convertData(array $input, string $entity_type_id, array $options): array {
    $output = [];

    if ($entity_type_id === 'user') {
      $output = $input;
      $output['pass'] = $options['simple-passwords'] ? '123456' : $this->passwordGenerator->generate();
      $output['status'] = 1;
    }

    elseif ($entity_type_id === 'lms_activity') {
      $output['uuid'] = $input['uuid'];
      $output['uid'] = $this->getValueFromUuid('user', $input['owner_uuid']);
      $output['type'] = $input['type'];
      $output += $input['values'];
    }

    elseif ($entity_type_id === 'lms_lesson') {
      $output['uuid'] = $input['uuid'];
      $output['uid'] = $this->getValueFromUuid('user', $input['owner_uuid']);
      $output += $input['values'];

      $output[LessonInterface::ACTIVITIES] = [];
      foreach ($input['activities'] as $activity_data) {
        $target_id = $this->getValueFromUuid('lms_activity', $activity_data['target_uuid']);
        unset($activity_data['target_uuid']);
        $new_activity_data = [
          'target_id' => $target_id,
          'data' => $activity_data,
        ];
        $output[LessonInterface::ACTIVITIES][] = $new_activity_data;
      }
    }

    elseif ($entity_type_id === 'group') {
      $output['uuid'] = $input['uuid'];
      $output['uid'] = $this->getValueFromUuid('user', $input['owner_uuid']);
      $output['type'] = 'lms_course';
      $output += $input['values'];

      $output[Course::LESSONS] = [];
      foreach ($input['lessons'] as $lesson_data) {
        $target_id = $this->getValueFromUuid('lms_lesson', $lesson_data['target_uuid']);
        unset($lesson_data['target_uuid']);
        $new_lesson_data = [
          'target_id' => $target_id,
          'data' => $lesson_data,
        ];
        $output[Course::LESSONS][] = $new_lesson_data;
      }
    }

    return $output;
  }

  /**
   * Helper method to get entity property from data array.
   */
  private function getValueFromUuid(string $entity_type_id, string $uuid, string $field_name = 'id', ?string $property = NULL): string {
    $entity_storage = $this->entityTypeManager->getStorage($entity_type_id);

    $query = $entity_storage->getQuery();
    $result = $query
      ->accessCheck(FALSE)
      ->condition('uuid', $uuid)
      ->execute();

    if (\count($result) === 0) {
      throw new \InvalidArgumentException(\sprintf("UUID %s doesn't exist in test data.", $uuid));
    }

    $id = \reset($result);
    if ($field_name === 'id') {
      return $id;
    }
    $entity = $entity_storage->load($id);
    \assert($entity instanceof ContentEntityInterface);
    if (!$entity->hasField($field_name)) {
      throw new \InvalidArgumentException(\sprintf("No %s field on entity.", $field_name));
    }
    $field = $entity->get($field_name);
    if ($field->isEmpty()) {
      throw new \InvalidArgumentException(\sprintf("Empty %s field on entity.", $field_name));
    }
    $item = $field->first();
    if ($property === NULL) {
      $property = $item::mainPropertyName();
    }
    return $item->get($property)->getValue();
  }

  /**
   * Reset course.
   */
  #[CLI\Command(name: 'lms:reset-course', aliases: ['lms-rc'])]
  #[CLI\Argument(name: 'course_id', description: 'The ID of the course to reset.')]
  #[CLI\Argument(name: 'user_id', description: 'The ID of the student to reset. If not provided, the entire course will be reset.')]
  #[CLI\Usage(name: 'drush lms-rc 1 3', description: 'Reset course 1 for user 3.')]
  public function resetCourse(
    string $course_id,
    ?string $user_id = NULL,
  ): void {
    $this->trainingManager->resetTraining($course_id, $user_id);

    $site_alias = $this->siteAliasManager()->getSelf();
    /** @var \Symfony\Component\Process\Process $process */
    $process = $this->processManager()->drush($site_alias, QueueCommands::RUN, ['lms_delete_entities']);
    $process->run($process->showRealtime()->hideStdout());

    if ($user_id === NULL) {
      $this->logger->success(\dt('Course has been reset for all users.'));
    }
    else {
      $this->logger->success(\dt('Course has been reset for the given user ID.'));
    }
  }

  /**
   * Validates lms:reset-course command arguments.
   */
  #[CLI\Hook(type: HookManager::ARGUMENT_VALIDATOR, target: 'lms:reset-course')]
  public function validateResetCourseArguments(CommandData $commandData): void {
    $args = $commandData->input()->getArguments();
    $course = $this->entityTypeManager->getStorage('group')->load($args['course_id']);
    if (!$course instanceof Course) {
      throw new \InvalidArgumentException("A course with ID {$args['course_id']} does not exist.");
    }
    if ($args['user_id'] !== NULL) {
      $account = $this->entityTypeManager->getStorage('user')->load($args['user_id']);
      if (!$account instanceof UserInterface) {
        throw new \InvalidArgumentException("A user with ID {$args['user_id']} does not exist.");
      }
    }
  }

}
