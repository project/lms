<?php

declare(strict_types=1);

namespace Drupal\lms\Controller;

use Drupal\Component\Render\FormattableMarkup;
use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Ajax\OpenModalDialogCommand;
use Drupal\Core\Cache\CacheableMetadata;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\lms\Entity\Answer;
use Drupal\lms\Entity\Bundle\Course;
use Drupal\lms\Entity\LessonStatusInterface;
use Drupal\lms\TrainingManager;
use Drupal\user\UserInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;

/**
 * The training controller.
 */
final class CourseController extends ControllerBase {

  use CourseControllerTrait;

  public function __construct(protected readonly TrainingManager $trainingManager) {}

  /**
   * Start the learning path.
   *
   * This page will redirect the user to the first learning path content.
   */
  public function start(Course $group): RedirectResponse {
    try {
      $lesson_status = $this->trainingManager->getRequestedLessonStatus($group, $this->currentUser());
    }
    catch (\Exception $e) {
      $url = $this->handleError($group, $e);
      return $this->redirect($url->getRouteName(), $url->getRouteParameters());
    }
    $route_parameters = [
      'group' => $group->id(),
      'lesson_delta' => $lesson_status->getCurrentLessonDelta(),
      'activity_delta' => $lesson_status->getCurrentActivityDelta(),
    ];

    return $this->redirect('lms.group.answer_form', $route_parameters);
  }

  /**
   * Returns lesson question answer form title.
   */
  public function activityFormTitle(Course $group, int $lesson_delta): string|TranslatableMarkup {
    try {
      $course_status = $this->trainingManager->getCurrentCourseStatus($group, $this->currentUser());
    }
    catch (\Exception $e) {
      return $this->t('Invalid request.');
    }

    $lesson = $group->getLesson($lesson_delta);
    if ($lesson === NULL) {
      return $this->t('Invalid lesson delta.');
    }

    $lesson_status = $this->trainingManager->loadLessonStatus($course_status->id(), $lesson->id());
    $activity = $lesson_status->getCurrentActivity();
    if ($activity === NULL) {
      return $lesson->label();
    }

    return $lesson->label() . ' - ' . $activity->label();
  }

  /**
   * Specific activity callback.
   */
  public function activity(
    Course $group,
    int $lesson_delta,
    int $activity_delta,
  ): array|RedirectResponse {
    try {
      $lesson_status = $this->trainingManager->getRequestedLessonStatus($group, $this->currentUser(), [
        'lesson' => $lesson_delta,
        'activity' => $activity_delta,
      ]);
    }
    catch (\Exception $e) {
      $url = $this->handleError($group, $e);
      return $this->redirect($url->getRouteName(), $url->getRouteParameters());
    }

    return $lesson_status->getAnswerForm();
  }

  /**
   * Results page title callback.
   */
  public function resultsTitle(Course $group, ?UserInterface $user = NULL): TranslatableMarkup {
    return $this->t('@course results for @user', [
      '@course' => $group->label(),
      '@user' => $user === NULL ? $this->getCurrentUser()->label() : $user->label(),
    ]);
  }

  /**
   * Results page callback.
   */
  public function results(Course $group, ?UserInterface $user = NULL): array {
    if ($user === NULL) {
      $user = $this->getCurrentUser();
    }
    $course_status = $this->trainingManager->loadCourseStatus($group, $user, [
      'current' => TRUE,
    ]);
    if ($course_status === NULL) {
      $completion_status = $this->t('Not started');
    }
    else {
      $completion_status = $course_status->getStatusAndScore();
    }
    $build = [];

    $build['course_status'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Completion status:'),
      'status' => [
        '#type' => 'html_tag',
        '#tag' => 'span',
        '#attributes' => ['data-lms-selector' => 'course-status'],
        '#value' => $completion_status,
      ],
    ];

    if ($course_status === NULL) {
      return $build;
    }

    $cacheability = new CacheableMetadata();
    $cacheability->addCacheableDependency($course_status);

    // If the current user is a member of the course, evaluation of
    // unevaluated activities should be possible from here.
    $is_teacher = $group->getMember($this->currentUser()) !== FALSE;
    if ($is_teacher === FALSE) {
      $is_teacher = $this->currentUser()->hasPermission('administer lms');
    }
    if ($is_teacher) {
      $build['#attached']['library'] = [
        'core/drupal.dialog.ajax',
      ];
    }

    $build['lessons'] = [];
    $lesson_statuses = $this->entityTypeManager()->getStorage('lms_lesson_status')->loadByProperties([
      'course_status' => $course_status->id(),
    ]);
    foreach ($lesson_statuses as $lesson_status) {
      \assert($lesson_status instanceof LessonStatusInterface);
      $lesson_status_id = $lesson_status->id();
      $build['lessons'][$lesson_status_id] = [
        '#type' => 'details',
        '#title' => new FormattableMarkup('<span data-lms-selector="lesson-status-@id">@status</span>', [
          '@id' => $lesson_status_id,
          '@status' => $this->getLessonStatusSummary($lesson_status),
        ]),
        '#open' => FALSE,
        '#attributes' => ['class' => ['lesson-score-details']],
      ];

      $lesson_status->buildResults($build['lessons'][$lesson_status_id], $is_teacher);

      $cacheability->addCacheableDependency($lesson_status);
    }

    $cacheability->applyTo($build);
    return $build;
  }

  /**
   * Open evaluation modal form.
   */
  public function openEvaluationForm(Answer $lms_answer, string $js): array|AjaxResponse {
    $use_ajax = ($js === 'ajax');

    $build = $this->entityFormBuilder()->getForm($lms_answer, 'evaluate', ['use_ajax' => $use_ajax]);

    if ($use_ajax) {
      $response = new AjaxResponse();
      $response->addCommand(new OpenModalDialogCommand($this->t('Evaluate @activity', [
        '@activity' => $lms_answer->getActivity()->label(),
      ]), $build, ['width' => '80%']));
      return $response;
    }

    return $build;
  }

  /**
   * Code saver.
   */
  private function getCurrentUser(): UserInterface {
    return $this->entityTypeManager()->getStorage('user')->load($this->currentUser()->id());
  }

}
