<?php

declare(strict_types=1);

namespace Drupal\lms\Entity\Bundle;

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\Core\Field\FieldStorageDefinitionInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\group\Access\GroupAccessResult;
use Drupal\group\Entity\Group;
use Drupal\group\Entity\GroupInterface;
use Drupal\lms\Entity\LessonInterface;
use Drupal\lms\Plugin\Field\FieldType\LMSReferenceItem;

/**
 * Course group bundle class.
 */
final class Course extends Group {

  public const LESSONS = 'lessons';

  /**
   * Get learning path classes.
   *
   * @return \Drupal\group\Entity\GroupInterface[]
   *   Class groups array.
   */
  public function getClasses(): array {
    $classes = [];
    $relationships = $this->getRelationships('lms_classes');
    foreach ($relationships as $relationship) {
      $class = $relationship->getEntity();
      \assert($class instanceof GroupInterface);
      $classes[] = $class;
    }
    return $classes;
  }

  /**
   * Class membership checker.
   *
   * Checks if the given account is a member of any of the classes of this
   * learning path.
   */
  public function isClassMember(AccountInterface $account): bool {
    $classes = $this->getClasses();

    foreach ($classes as $class) {
      if ($class->getMember($account) !== FALSE) {
        return TRUE;
      }
    }

    return FALSE;
  }

  /**
   * Check if the given user can take this learning path.
   */
  public function takeAccess(AccountInterface $account): AccessResult {
    // Only grant access to course members and class members.
    if ($this->getMember($account) !== FALSE) {
      // Allow access to any member of the learning path that can edit group
      // for testing purposes.
      return GroupAccessResult::allowedIfHasGroupPermission($this, $account, 'edit group');
    }

    // Or if the user is a member of any class. In such a case we need
    // to add a specific cache invalidation tags.
    return AccessResult::allowedIf($this->isClassMember($account))
      ->addCacheableDependency($this)
      ->addCacheTags([
        'lms_memberships:' . $account->id(),
        'group.classes:' . $this->id(),
      ])
      ->cachePerUser();
  }

  /**
   * Check if group has guided navigation enabled.
   */
  public function hasFreeNavigation(): bool {
    if (!$this->hasField('free_navigation')) {
      return FALSE;
    }
    return (bool) $this->get('free_navigation')->value;
  }

  /**
   * Get loading conditions when starting / continuing a training.
   *
   * If no course status meeting the given conditions is found, training will be
   * (re) initialized.
   *
   * @return mixed[]
   *   Conditions for loading status entity when starting a course.
   */
  public function getInitialCourseStatusConditions(): array {
    $conditions = [];
    // If revisit mode is disabled only in progress course can be entered.
    if (!$this->revisitMode()) {
      $conditions['finished'] = 0;
    }
    return $conditions;
  }

  /**
   * Check wether the course has revisit mode enabled.
   */
  public function revisitMode(): bool {
    return (bool) $this->get('revisit_mode')->value;
  }

  /**
   * Get lesson.
   */
  public function getLesson(int $lesson_delta): ?LessonInterface {
    $item = $this->getLessonItem($lesson_delta);
    $lesson = $item->get('entity')->getValue();
    if (!$lesson instanceof LessonInterface) {
      return NULL;
    }

    return $lesson;
  }

  /**
   * Get Lesson field item.
   */
  public function getLessonItem(int $lesson_delta): ?LMSReferenceItem {
    $field = $this->get(self::LESSONS);
    if (
      $field->isEmpty() ||
      !$field->offsetExists($lesson_delta)
    ) {
      return NULL;
    }
    /** @var \Drupal\lms\Plugin\Field\FieldType\LMSReferenceItem */
    return $field->get($lesson_delta);
  }

  /**
   * {@inheritdoc}
   */
  public static function bundleFieldDefinitions(EntityTypeInterface $entity_type, mixed $bundle, array $base_field_definitions): array {
    $fields = parent::bundleFieldDefinitions($entity_type, $bundle, $base_field_definitions);

    $fields[self::LESSONS] = BaseFieldDefinition::create('lms_reference')
      ->setLabel(\t('Lessons of this course'))
      ->setDescription(\t('Lesson entity references plus data specific for the lesson.'))
      ->setRevisionable(TRUE)
      ->setSetting('target_type', 'lms_lesson')
      ->setCardinality(FieldStorageDefinitionInterface::CARDINALITY_UNLIMITED)
      ->setDisplayOptions('form', [
        'type' => 'lms_reference_table',
        'settings' => [
          'view_data' => 'lessons_selection.default',
        ],
        'weight' => 1,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->addConstraint('NotNull', [
        'message' => new TranslatableMarkup('At least one lesson needs to be a part of the course.'),
      ]);

    $fields['revisit_mode'] = BaseFieldDefinition::create('boolean')
      ->setLabel(new TranslatableMarkup('Allow to revisit finished course'))
      ->setDescription(new TranslatableMarkup('Always allow to go back to the training, navigate freely and correct answers without the need to restart.'))
      ->setRevisionable(TRUE)
      ->setTranslatable(FALSE)
      ->setDefaultValue(TRUE)
      ->setDisplayOptions('form', [
        'type' => 'boolean_checkbox',
        'settings' => [
          'display_label' => TRUE,
        ],
        'weight' => 2,
      ])
      ->setDisplayConfigurable('form', TRUE);
    $fields['free_navigation'] = BaseFieldDefinition::create('boolean')
      ->setLabel(new TranslatableMarkup('Free navigation'))
      ->setDescription(new TranslatableMarkup('Check this to allow students to go back in a course or jump to any activity (always allowed when revisiting).'))
      ->setDefaultValue(FALSE)
      ->setRevisionable(TRUE)
      ->setTranslatable(FALSE)
      ->setDisplayOptions('form', [
        'type' => 'boolean_checkbox',
        'settings' => [
          'display_label' => TRUE,
        ],
        'weight' => 3,
      ])
      ->setDisplayConfigurable('form', TRUE);

    return $fields;
  }

}
