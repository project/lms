<?php

declare(strict_types=1);

namespace Drupal\lms\Entity\Form;

use Drupal\Core\Entity\ContentEntityForm;
use Drupal\Core\Form\FormStateInterface;
use Drupal\lms\Form\Modal\ModalEntitySubformTrait;

/**
 * Activity add / edit form.
 */
final class ActivityForm extends ContentEntityForm {

  use ModalEntitySubformTrait;

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state): array {
    $form = parent::buildForm($form, $form_state);
    $this->adaptFormToModal($form, $form_state);

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state): int {
    $is_new = $this->entity->isNew();
    $result = parent::save($form, $form_state);

    if ($this->modalSave($form_state)) {
      return $result;
    }

    if ($is_new) {
      $this->messenger()->addStatus($this->t('Activity @label has been created.', [
        '@label' => $this->entity->label(),
      ]));
    }
    else {
      $this->messenger()->addStatus($this->t('Activity @label has been updated.', [
        '@label' => $this->entity->label(),
      ]));
    }

    $form_state->setRedirect('entity.lms_activity.collection');

    return $result;
  }

}
