<?php

declare(strict_types=1);

namespace Drupal\lms\Entity\Form;

use Drupal\Component\Datetime\TimeInterface;
use Drupal\Core\Entity\ContentEntityForm;
use Drupal\Core\Entity\EntityRepositoryInterface;
use Drupal\Core\Entity\EntityTypeBundleInfoInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;
use Drupal\lms\ActivityAnswerManager;
use Drupal\lms\Controller\CourseControllerTrait;
use Drupal\lms\Entity\AnswerInterface;
use Drupal\lms\Entity\LessonStatusInterface;
use Drupal\lms\Form\AnswerFormTrait;
use Drupal\lms\TrainingManager;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;

/**
 * Form controller for Answer edit forms.
 */
final class AnswerForm extends ContentEntityForm {

  use CourseControllerTrait;
  use AnswerFormTrait;

  public function __construct(
    EntityRepositoryInterface $entity_repository,
    EntityTypeBundleInfoInterface $entity_type_bundle_info,
    TimeInterface $time,
    protected ActivityAnswerManager $answerManager,
    protected TrainingManager $trainingManager,
  ) {
    parent::__construct($entity_repository, $entity_type_bundle_info, $time);
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container): self {
    return new static(
      $container->get('entity.repository'),
      $container->get('entity_type.bundle.info'),
      $container->get('datetime.time'),
      $container->get('plugin.manager.activity_answer'),
      $container->get('lms.training_manager')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildForm($form, $form_state);

    \assert($this->entity instanceof AnswerInterface);
    $lesson_status = $this->entity->getLessonStatus();

    // Check if lesson status is not over time.
    $redirect_url = $this->overTimeHandler($lesson_status);
    if ($redirect_url !== NULL) {
      // Form builder can actually return a redirect response.
      // @phpstan-ignore return.type
      return new RedirectResponse($redirect_url->toString(), 303);
    }

    $close_time = $lesson_status->getCloseTime();
    if ($close_time !== 0) {
      $form['lms_lesson_timer'] = [
        '#theme' => 'lms_lesson_timer',
        '#close_time' => $close_time,
        '#attached' => [
          'library' => [
            'lms/lessonTimer',
          ],
        ],
      ];
    }

    // Hide revision_log_message field.
    unset($form['revision_log_message']);

    $activity = $this->entity->getActivity();

    // Backwards navigation.
    $form['actions']['back'] = [
      '#type' => 'submit',
      '#value' => $this->t('Back'),
      '#submit' => [
        '::backwardsNavigation',
      ],
      '#access' => FALSE,
    ];

    // Conditionally allow backwards navigation.
    $back_parameters = $this->trainingManager->getBackNavParameters($this->entity);
    if (\count($back_parameters) !== 0) {
      $form['actions']['back']['#access'] = TRUE;
    }

    $elements_count = \count($form);

    // Add activity specific form elements.
    $plugin_id = $this->trainingManager->getActivityPluginId($activity);
    if ($plugin_id !== NULL && $this->answerManager->hasDefinition($plugin_id)) {
      $plugin = $this->answerManager->createInstance($plugin_id);
      $plugin->answeringForm($form, $form_state, $this->entity);
    }

    // Actions tweaks.
    $form['actions']['submit']['#submit'] = ['::submitForm'];
    $form['actions']['submit']['#value'] = $this->t('Submit');
    // Last activity of the course.
    if ($lesson_status->getNextActivityDelta() === NULL && $lesson_status->getNextLessonDelta() === NULL) {
      $form['actions']['submit']['#value'] = $this->t('Finish course');
    }
    // No answer form elements - information only activity.
    elseif ($elements_count === \count($form)) {
      $form['actions']['submit']['#value'] = $this->t('Next');
    }

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state): void {
    \assert($this->entity instanceof AnswerInterface);
    $activity = $this->entity->getActivity();
    $lesson_status = $this->entity->getLessonStatus();

    // Check if lesson status is not over time.
    $redirect_url = $this->overTimeHandler($lesson_status);
    if ($redirect_url !== NULL) {
      $form_state->setRedirectUrl($redirect_url);
      return;
    }

    $lesson = $lesson_status->getLesson();
    $course_status = $lesson_status->getCourseStatus();

    $max_score = $this->trainingManager->getActivityMaxScore($lesson, $activity);

    // Determine score and evaluated property for the answer.
    $form_state->addCleanValueKey('langcode');
    $form_state->addCleanValueKey('back');
    $form_state->cleanValues();
    $this->entity->setData($form_state->getValues());

    $plugin_id = $this->trainingManager->getActivityPluginId($activity);
    if ($plugin_id !== NULL && $this->answerManager->hasDefinition($plugin_id)) {
      $plugin = $this->answerManager->createInstance($plugin_id);
      if ($plugin->evaluatedOnSave($this->entity)) {
        $score = $plugin->getScore($this->entity) * $max_score;
        $evaluated = TRUE;
      }
      else {
        // Always set unevaluated score to max to avoid blocking user from
        // going to next lessons.
        $score = $max_score;
        $evaluated = FALSE;
        $lesson_status->setEvaluated(FALSE);
      }
    }
    // No plugin, just config - static display-only activity.
    else {
      $evaluated = TRUE;
      $score = $max_score;
    }

    $this->entity
      ->setEvaluated($evaluated)
      ->setScore($score)
      ->save();

    // Set latest user activity time on Course status.
    $this->trainingManager->setLastActivityTime($course_status);

    // Proceed to the next activity.
    $next_activity_delta = $lesson_status->getNextActivityDelta();
    $next_lesson_status = NULL;
    if ($next_activity_delta === NULL) {
      $this->trainingManager->updateLessonStatus($lesson_status);
      try {
        $next_lesson_status = $this->trainingManager->getNextLessonStatus($course_status);
      }
      catch (\Exception $e) {
        $this->trainingManager->updateCourseStatus($course_status);
        $url = $this->handleError($course_status->getCourse(), $e);
        $form_state->setRedirectUrl($url);
        return;
      }

      if ($next_lesson_status !== NULL) {
        $next_lesson_status->save();
        $next_activity_delta = $next_lesson_status->getCurrentActivityDelta();
        $course_status->set('current_lesson_status', $next_lesson_status);
        $lesson_status = $next_lesson_status;
      }

      // We have all required properties set on Course Status now,
      // we can recalculate and save.
      $last_activity = (
        $next_activity_delta === NULL &&
        $next_lesson_status === NULL
      );
      $this->trainingManager->updateCourseStatus($course_status, $last_activity);
    }
    else {
      $lesson_status->setCurrentActivityDelta($next_activity_delta);
      $lesson_status->save();
      // We also need to save Course status to update last activity time.
      $course_status->save();
    }

    if ($next_activity_delta === NULL) {
      $url = $this->handleFinishedCourse($course_status);
      $form_state->setRedirectUrl($url);
      return;
    }

    $form_state->setRedirect('lms.group.answer_form', [
      'group' => $course_status->get('gid')->target_id,
      'lesson_delta' => $lesson_status->getCurrentLessonDelta(),
      'activity_delta' => $lesson_status->getCurrentActivityDelta(),
    ]);
  }

  /**
   * {@inheritdoc}
   */
  public function backwardsNavigation(array $form, FormStateInterface $form_state): void {
    \assert($this->entity instanceof AnswerInterface);
    $course_status = $this->entity->getLessonStatus()->getCourseStatus();
    $back_parameters = $this->trainingManager->getBackNavParameters($this->entity);

    try {
      $lesson_status = $this->trainingManager->goBackwards($course_status, $back_parameters);
    }
    catch (\Exception $e) {
      $url = $this->handleError($course_status->getCourse(), $e);
      $form_state->setRedirectUrl($url);
      return;
    }

    $form_state->setRedirect('lms.group.answer_form', [
      'group' => $course_status->get('gid')->target_id,
      'lesson_delta' => $lesson_status->getCurrentLessonDelta(),
      'activity_delta' => $lesson_status->getCurrentActivityDelta(),
    ]);
  }

  /**
   * Handle overtime.
   */
  private function overTimeHandler(LessonStatusInterface $lesson_status): ?Url {
    if (!$lesson_status->isOverTime()) {
      return NULL;
    }

    $lesson_status->setFinished();
    $this->trainingManager->updateLessonStatus($lesson_status);
    $course_status = $lesson_status->getCourseStatus();
    try {
      $next_lesson_status = $this->trainingManager->getNextLessonStatus($course_status);
    }
    catch (\Exception $e) {
      $this->messenger()->addError('Lesson is over time.');
      $this->trainingManager->updateCourseStatus($course_status);
      return $this->handleError($course_status->getCourse(), $e);
    }

    if ($next_lesson_status !== NULL) {
      $this->messenger()->addError("Lesson is over time, you've been redirected to the next lesson.");
      $next_lesson_status->save();
      $course_status->set('current_lesson_status', $next_lesson_status);
      $this->trainingManager->updateCourseStatus($course_status);
      return Url::fromRoute('lms.group.answer_form', [
        'group' => $course_status->get('gid')->target_id,
        'lesson_delta' => $next_lesson_status->getCurrentLessonDelta(),
        'activity_delta' => $next_lesson_status->getCurrentActivityDelta(),
      ]);
    }
    else {
      $this->messenger()->addError('Lesson is over time, course finished.');
      $this->trainingManager->updateCourseStatus($course_status, TRUE);
      return Url::fromRoute('entity.group.canonical', [
        'group' => $course_status->get('gid')->target_id,
      ]);
    }

  }

}
