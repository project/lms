<?php

declare(strict_types=1);

namespace Drupal\lms\Entity;

use Drupal\Core\Entity\EntityChangedTrait;
use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Entity\RevisionableContentEntityBase;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\user\EntityOwnerTrait;

/**
 * Defines the Activity entity.
 *
 * @ContentEntityType(
 *   id = "lms_activity",
 *   label = @Translation("Activity"),
 *   bundle_label = @Translation("Activity type"),
 *   handlers = {
 *     "view_builder" = "Drupal\Core\Entity\EntityViewBuilder",
 *     "list_builder" = "Drupal\lms\Entity\Handlers\ActivityListBuilder",
 *     "form" = {
 *       "default" = "Drupal\lms\Entity\Form\ActivityForm",
 *       "add" = "Drupal\lms\Entity\Form\ActivityForm",
 *       "edit" = "Drupal\lms\Entity\Form\ActivityForm",
 *       "delete" = "Drupal\lms\Entity\Form\ActivityDeleteForm",
 *     },
 *     "access" = "Drupal\lms\Entity\Handlers\ActivityAccessControlHandler",
 *     "route_provider" = {
 *       "html" = "Drupal\Core\Entity\Routing\DefaultHtmlRouteProvider",
 *     },
 *     "views_data" = "Drupal\lms\Entity\Handlers\LmsEntityViewsDataProvider",
 *   },
 *   base_table = "lms_activity",
 *   data_table = "lms_activity_field_data",
 *   revision_table = "lms_activity_revision",
 *   revision_data_table = "lms_activity_field_revision",
 *   translatable = TRUE,
 *   show_revision_ui = TRUE,
 *   admin_permission = "administer lms",
 *   collection_permission = "create lms_activity entities",
 *   entity_keys = {
 *     "id" = "id",
 *     "revision" = "vid",
 *     "bundle" = "type",
 *     "label" = "name",
 *     "uuid" = "uuid",
 *     "uid" = "uid",
 *     "owner" = "uid",
 *     "langcode" = "langcode"
 *   },
 *   revision_metadata_keys = {
 *     "revision_user" = "revision_user",
 *     "revision_created" = "revision_created",
 *     "revision_log_message" = "revision_log_message",
 *   },
 *   links = {
 *     "canonical" = "/admin/lms/activity/{lms_activity}",
 *     "add-page" = "/admin/lms/activity/add",
 *     "add-form" = "/admin/lms/activity/add/{lms_activity_type}",
 *     "edit-form" = "/admin/lms/activity/{lms_activity}/edit",
 *     "delete-form" = "/admin/lms/activity/{lms_activity}/delete",
 *     "collection" = "/admin/lms/activity",
 *     "version-history" = "/admin/lms/activity/{lms_activity}/revisions",
 *     "revision-revert" = "/admin/lms/activity/{lms_activity}/revision/{lms_activity_revision}/revert",
 *     "revision-delete" = "/admin/lms/activity/{lms_activity}/revision/{lms_activity_revision}/delete",
 *     "revision-preview" = "/admin/lms/activity/{lms_activity}/revision/{lms_activity_revision}/view",
 *   },
 *   bundle_entity_type = "lms_activity_type",
 *   field_ui_base_route = "entity.lms_activity_type.edit_form"
 * )
 */
class Activity extends RevisionableContentEntityBase implements ActivityInterface {

  use EntityChangedTrait;
  use EntityOwnerTrait;

  /**
   * {@inheritdoc}
   */
  public static function preCreate(EntityStorageInterface $storage_controller, array &$values): void {
    parent::preCreate($storage_controller, $values);
    $values += [
      'uid' => \Drupal::currentUser()->id(),
    ];
  }

  /**
   * {@inheritdoc}
   */
  public static function baseFieldDefinitions(EntityTypeInterface $entity_type) {
    $fields = parent::baseFieldDefinitions($entity_type);

    $fields['uid'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(\t('Authored by'))
      ->setDescription(\t('The user ID of author of the Activity entity.'))
      ->setRevisionable(TRUE)
      ->setSetting('target_type', 'user')
      ->setSetting('handler', 'default')
      ->setTranslatable(TRUE)
      ->setDisplayOptions('view', [
        'label' => 'hidden',
        'type' => 'author',
        'weight' => 0,
      ])
      ->setDisplayOptions('form', [
        'type' => 'entity_reference_autocomplete',
        'weight' => 5,
        'settings' => [
          'match_operator' => 'CONTAINS',
          'size' => '60',
          'autocomplete_type' => 'tags',
          'placeholder' => '',
        ],
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['name'] = BaseFieldDefinition::create('string')
      ->setLabel(\t('Name'))
      ->setDescription(\t('The name of the Activity entity.'))
      ->setRequired(TRUE)
      ->setRevisionable(TRUE)
      ->setTranslatable(TRUE)
      ->setSettings([
        'max_length' => 255,
        'text_processing' => 0,
      ])
      ->setDefaultValue('')
      ->setDisplayOptions('view', [
        'label' => 'above',
        'type' => 'string',
        'weight' => -4,
      ])
      ->setDisplayOptions('form', [
        'type' => 'string_textfield',
        'weight' => -4,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['created'] = BaseFieldDefinition::create('created')
      ->setLabel(\t('Authored on'))
      ->setDescription(\t('The time that the Activity was created.'))
      ->setRevisionable(TRUE)
      ->setTranslatable(TRUE)
      ->setDisplayOptions('form', [
        'type' => 'datetime_timestamp',
        'weight' => 10,
      ])
      ->setDisplayConfigurable('form', TRUE);

    $fields['changed'] = BaseFieldDefinition::create('changed')
      ->setLabel(\t('Changed'))
      ->setDescription(\t('The time that the Activity was last edited.'))
      ->setRevisionable(TRUE)
      ->setTranslatable(TRUE);

    return $fields;
  }

}
