<?php

declare(strict_types=1);

namespace Drupal\lms\Entity\Handlers;

/**
 * Access controller for the Activity entity.
 *
 * @see \Drupal\lms\Entity\Activity.
 */
final class ActivityAccessControlHandler extends LmsEntityAccessControlHandlerBase {
  public const CREATE_PERMISSION = 'create lms_activity entities';
  public const EDIT_PERMISSION = 'edit lms_activity entities';

}
