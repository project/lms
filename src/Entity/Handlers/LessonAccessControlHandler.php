<?php

declare(strict_types=1);

namespace Drupal\lms\Entity\Handlers;

/**
 * Access controller for the Lesson entity.
 *
 * @see \Drupal\lms\Entity\Lesson.
 */
final class LessonAccessControlHandler extends LmsEntityAccessControlHandlerBase {

  /**
   * The string value of the create permission.
   */
  public const CREATE_PERMISSION = 'create lms_lesson entities';

  /**
   * The string value of the edit permission.
   */
  public const EDIT_PERMISSION = 'edit lms_lesson entities';

}
