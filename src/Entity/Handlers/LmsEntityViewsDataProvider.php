<?php

declare(strict_types=1);

namespace Drupal\lms\Entity\Handlers;

use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\views\EntityViewsData;

/**
 * Provides Views data for LMS entity types (Activities, Lessons).
 */
final class LmsEntityViewsDataProvider extends EntityViewsData {

  use StringTranslationTrait;

  /**
   * {@inheritdoc}
   */
  public function getViewsData(): array {
    $data = parent::getViewsData();

    $data[$this->entityType->getBaseTable()]['lms_select'] = [
      'title' => $this->t('LMS selection for adding to parent'),
      'help' => $this->t('Add a checkbox for every row - required to be able to add @label to parent entity in an entity form widget.', [
        '@label' => $this->entityType->getPluralLabel(),
      ]),
      'field' => [
        'id' => 'lms_entity_selection',
        'real field' => $this->entityType->getKey('id'),
      ],
    ];

    return $data;
  }

}
