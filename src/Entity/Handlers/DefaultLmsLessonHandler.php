<?php

declare(strict_types=1);

namespace Drupal\lms\Entity\Handlers;

/**
 * Handler for standard lessons.
 */
final class DefaultLmsLessonHandler extends LmsLessonHandlerBase {

}
