<?php

declare(strict_types=1);

namespace Drupal\lms\Entity\Handlers;

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Access\AccessResultInterface;
use Drupal\Core\Entity\EntityAccessControlHandler;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\user\EntityOwnerInterface;

/**
 * Base class for editable LMS entities.
 */
abstract class LmsEntityAccessControlHandlerBase extends EntityAccessControlHandler {

  public const CREATE_PERMISSION = '';
  public const EDIT_PERMISSION = '';

  /**
   * {@inheritdoc}
   */
  protected function checkAccess(EntityInterface $entity, $operation, AccountInterface $account): AccessResultInterface {
    if (
      $entity instanceof EntityOwnerInterface &&
      $entity->getOwnerId() === $account->id()
    ) {
      return AccessResult::allowedIfHasPermissions($account, [
        'administer lms',
        static::CREATE_PERMISSION,
      ], 'OR')->cachePerUser()->addCacheableDependency($entity);
    }

    return AccessResult::allowedIfHasPermissions($account, [
      'administer lms',
      static::EDIT_PERMISSION,
    ], 'OR');
  }

  /**
   * {@inheritdoc}
   */
  protected function checkCreateAccess(AccountInterface $account, array $context, $entity_bundle = NULL): AccessResultInterface {
    return AccessResult::allowedIfHasPermissions($account, [
      'administer lms',
      static::CREATE_PERMISSION,
    ], 'OR');
  }

  /**
   * {@inheritdoc}
   */
  protected function checkFieldAccess($operation, FieldDefinitionInterface $field_definition, AccountInterface $account, ?FieldItemListInterface $items = NULL): AccessResultInterface {
    if ($operation !== 'edit') {
      return parent::checkFieldAccess($operation, $field_definition, $account, $items);
    }

    // Only users with the administer nodes permission can edit administrative
    // fields.
    $administrative_fields = ['uid', 'created', 'changed', 'revision_log'];
    if (\in_array($field_definition->getName(), $administrative_fields, TRUE)) {
      return AccessResult::allowedIfHasPermission($account, 'administer lms');
    }

    // No user can change read only fields.
    $read_only_fields = ['revision_timestamp', 'revision_uid'];
    if (\in_array($field_definition->getName(), $read_only_fields, TRUE)) {
      return AccessResult::forbidden();
    }

    return parent::checkFieldAccess($operation, $field_definition, $account, $items);
  }

}
