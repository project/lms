<?php

declare(strict_types=1);

namespace Drupal\lms\Entity\Handlers;

use Drupal\lms\Entity\CourseStatus;
use Drupal\views\EntityViewsData;

/**
 * Views data handler for Course Status entity type.
 */
final class CourseStatusViewsData extends EntityViewsData {

  /**
   * {@inheritdoc}
   */
  public function getViewsData(): array {
    $data = parent::getViewsData();

    $data['lms_course_status']['class_parent_or_current_group'] = [
      'help' => $this->t('Class parent group statuses for the class members only or learning path statuses'),
      'argument' => [
        'title' => $this->t('Class member or all classes` members statuses'),
        'id' => 'class_parent_or_current_group',
      ],
    ];

    $data['lms_course_status']['status']['field']['id'] = 'lms_student_course_status';

    $data['lms_course_status']['status']['filter'] = [
      'id' => 'in_operator',
      'options callback' => [CourseStatus::class, 'getStatusOptions'],
    ];

    return $data;
  }

}
