<?php

declare(strict_types=1);

namespace Drupal\lms\Plugin\Block;

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Block\Attribute\Block;
use Drupal\Core\Block\BlockBase;
use Drupal\Core\Cache\Cache;
use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Cache\CacheableMetadata;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Plugin\Context\EntityContextDefinition;
use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\Core\Url;
use Drupal\lms\Entity\Bundle\Course;
use Drupal\lms\Entity\LessonInterface;
use Drupal\lms\TrainingManager;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a course navigation block.
 *
 * @todo Drop the legacy themes in favor of new, simple ones.
 */
#[Block(
  id: 'course_steps_block',
  admin_label: new TranslatableMarkup('Course navigation'),
  context_definitions: [
    'user' => new EntityContextDefinition('entity:user', new TranslatableMarkup('User')),
    'group' => new EntityContextDefinition('entity:group', new TranslatableMarkup('Group')),
  ]
)]
final class StepsBlock extends BlockBase implements ContainerFactoryPluginInterface {

  private const CACHE_PREFIX = 'steps_block_data_cache_';

  /**
   * Static cache of user lesson statuses.
   */
  private ?array $lessonStatuses = NULL;

  /**
   * Static cache of route parameters.
   */
  private ?array $routeParameters = NULL;

  /**
   * {@inheritdoc}
   */
  public function __construct(
    array $configuration,
    $plugin_id,
    $plugin_definition,
    protected readonly AccountInterface $currentUser,
    protected readonly CacheBackendInterface $cache,
    protected readonly TrainingManager $trainingManager,
    protected readonly RouteMatchInterface $currentRouteMatch,
  ) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
  }

  /**
   * {@inheritdoc}
   */
  public static function create(
    ContainerInterface $container,
    array $configuration,
    $plugin_id,
    $plugin_definition,
  ) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('current_user'),
      $container->get('cache_factory')->get('data'),
      $container->get('lms.training_manager'),
      $container->get('current_route_match')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getCacheMaxAge() {
    // This block changes on almost every request except navigation
    // to already completed activities which happens rarely.
    // @todo Implement marking of current lesson and current activity
    // in frontend, then we'll be able to cache this block and invalidate
    // when a lesson status is updated / created for this group and user
    // (custom cache tag). Changes to training itself will be covered after
    // we will implement status cleanup upon making such changes.
    return 0;
  }

  /**
   * Get route parameter.
   */
  private function getRouteParameter(string $parameter): ?int {
    if ($this->routeParameters === NULL) {
      $this->routeParameters = [];
      foreach (['lesson_delta', 'activity_delta'] as $param) {
        $this->routeParameters[$param] = $this->currentRouteMatch->getParameter($param);
      }
    }
    return \array_key_exists($parameter, $this->routeParameters) ? (int) $this->routeParameters[$parameter] : NULL;
  }

  /**
   * Set lesson statuses.
   */
  private function setLessonStatuses(Course $group, array $lesson_ids): void {
    if ($this->lessonStatuses !== NULL) {
      return;
    }
    $this->lessonStatuses = [];

    $course_status = $this->trainingManager->loadCourseStatus($group, $this->getContextValue('user'), [
      'current' => TRUE,
    ]);
    if ($course_status === NULL) {
      return;
    }

    $lesson_statuses = $this->trainingManager->loadLessonStatusMultiple($course_status->id(), $lesson_ids);
    foreach ($lesson_statuses as $lesson_status) {
      $this->lessonStatuses[$lesson_status->get('lesson')->target_id] = $lesson_status;
    }
  }

  /**
   * Get data structure for this training.
   */
  private function getStructure(Course $group): array {
    $cid = self::CACHE_PREFIX . $group->id();
    $cache = $this->cache->get($cid);
    if ($cache !== FALSE) {
      $step_data = $cache->data;
    }
    else {
      $step_data = $this->trainingManager->getOrderedLessons($group);
      $invalidate_tags = ['group:' . $group->id()];
      foreach (\array_keys($step_data) as $lesson_id) {
        $invalidate_tags[] = 'lms_lesson:' . $lesson_id;
      }
      $this->cache->set($cid, $step_data, Cache::PERMANENT, $invalidate_tags);
    }

    return $step_data;
  }

  /**
   * {@inheritdoc}
   */
  public function build() {
    $group = $this->getContextValue('group');
    if (!$group instanceof Course) {
      return [];
    }

    $structure = $this->getStructure($group);
    $this->setLessonStatuses($group, \array_keys($structure));

    $renderable = [
      '#type' => 'container',
      [
        '#theme' => 'lms_lp_step_activity',
        'title' => [
          '#markup' => $group->label(),
        ],
        'steps' => [],
      ],
    ];

    foreach ($structure as $lesson_id => $step_data) {
      $lesson_id = (string) $lesson_id;
      $step_renderable = [
        '#theme' => 'lms_lp_step_module',
        '#label' => $step_data['label'],
        '#state' => 'pending',
        '#current' => FALSE,
        '#link' => Url::fromRoute('lms.group.answer_form', [
          'group' => $group->id(),
          'lesson_delta' => $step_data['delta'],
        ])->toString(),
        '#locked' => \count($step_data['activities']) === 0,
        'activities' => [],
      ];

      $cache_tags = ['lms_lesson:' . $lesson_id];

      if (\array_key_exists($lesson_id, $this->lessonStatuses)) {
        $this->applyLessonStatusData($step_renderable, $step_data, $group->id(), $lesson_id, $step_data['delta']);
      }
      elseif (\count($step_data['activities']) !== 0) {
        foreach (\array_keys($step_data['activities']) as $activity_id) {
          $cache_tags[] = 'lms_activity:' . $activity_id;
        }

        $this->mergeActivityRenderables($step_renderable['activities'], $step_data['activities'], $group->id(), $step_data['delta']);
        $cacheability = new CacheableMetadata();
        $cacheability->addCacheTags($cache_tags);
        $cacheability->applyTo($step_renderable);
      }

      $renderable[0]['steps'][] = $step_renderable;
    }

    return $renderable;
  }

  /**
   * Merge activity renderable to activities renderable.
   */
  private function mergeActivityRenderables(array &$activities, array $activities_data, string $group_id, int $step_delta): void {
    foreach ($activities_data as $activity_id => $activity_data) {
      $activities[$activity_id] = [
        '#theme' => 'lms_lp_step_module_activity',
        '#state' => $activity_data['state'] ?? 'pending',
        '#current' => $activity_data['current'] ?? FALSE,
        '#link' => Url::fromRoute('lms.group.answer_form', [
          'group' => $group_id,
          'lesson_delta' => $step_delta,
          'activity_delta' => $activity_data['delta'],
        ])->toString(),
        '#title' => $activity_data['label'],
        '#activity' => $activity_data['activity'],
      ];
    }
  }

  /**
   * Apply lesson status data to lesson renderable.
   */
  private function applyLessonStatusData(array &$step_renderable, array $step_data, string $group_id, string $lesson_id, int $step_delta): void {
    if (!\array_key_exists($lesson_id, $this->lessonStatuses)) {
      return;
    }
    $lesson_status = $this->lessonStatuses[$lesson_id];

    $activities_field = $lesson_status->get(LessonInterface::ACTIVITIES);
    if ($activities_field->isEmpty()) {
      return;
    }

    if ($lesson_status->isFinished()) {
      if ($lesson_status->getScore() >= $lesson_status->getRequiredScore()) {
        $step_renderable['#state'] = 'passed';
      }
      else {
        $step_renderable['#state'] = 'failed';
      }
    }

    $step_renderable['#current'] = $step_delta === $this->getRouteParameter('lesson_delta');
    $step_renderable['#locked'] = FALSE;

    $activities_data = [];
    $activity_ids = [];
    foreach ($activities_field as $activity_item) {
      $activity_ids[$activity_item->target_id] = $activity_item->target_id;
    }
    $answers = $this->trainingManager->getAnswersByActivityId($lesson_status, $activity_ids);

    foreach ($activities_field as $activity_delta => $activity_item) {
      $activity = $activity_item->entity;
      if ($activity === NULL) {
        continue;
      }
      $activities_data[$activity->id()] = [
        'label' => $activity->label(),
        'state' => \array_key_exists($activity->id(), $answers) ? 'passed' : 'pending',
        'current' => $step_renderable['#current'] && ($activity_delta === $this->getRouteParameter('activity_delta')),
        'activity' => $activity,
        'delta' => $activity_delta,
      ];
    }

    $this->mergeActivityRenderables($step_renderable['activities'], $activities_data, $group_id, $step_delta);
  }

  /**
   * {@inheritdoc}
   */
  protected function blockAccess(AccountInterface $account) {
    // Available only on answer form routes.
    if ($this->currentRouteMatch->getRouteName() === 'lms.group.answer_form') {
      $result = AccessResult::allowed();
    }
    else {
      $result = AccessResult::forbidden();
    }
    return $result->addCacheContexts(['route.name']);
  }

}
