<?php

declare(strict_types=1);

namespace Drupal\lms\Plugin;

use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Plugin\PluginBase;
use Drupal\lms\Config\PluginConfigInstaller;
use Drupal\lms\Entity\ActivityType;
use Drupal\lms\Entity\Answer;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Activity-answer plugin base class.
 */
abstract class ActivityAnswerBase extends PluginBase implements ActivityAnswerInterface, ContainerFactoryPluginInterface {

  /**
   * The plugin config installer.
   *
   * @var \Drupal\lms\Config\PluginConfigInstaller
   */
  protected PluginConfigInstaller $pluginConfigInstaller;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected EntityTypeManagerInterface $entityTypeManager;

  /**
   * Injects services.
   *
   * @param \Symfony\Component\DependencyInjection\ContainerInterface $container
   *   The dependency injection container.
   */
  public function injectServices(ContainerInterface $container): void {
    $this->pluginConfigInstaller = $container->get('plugin.config_installer.activity_answer');
    $this->entityTypeManager = $container->get('entity_type.manager');
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    $instance = new static($configuration, $plugin_id, $plugin_definition);
    $instance->injectServices($container);
    return $instance;
  }

  /**
   * {@inheritdoc}
   */
  public function getId(): string {
    return $this->pluginDefinition['id'];
  }

  /**
   * {@inheritdoc}
   */
  public function evaluatedOnSave(Answer $answer): bool {
    return TRUE;
  }

  /**
   * {@inheritdoc}
   */
  public function getScore(Answer $answer): float {
    return 1;
  }

  /**
   * {@inheritdoc}
   */
  public function answeringForm(array &$form, FormStateInterface $form_state, Answer $answer): void {
    // No additional form elements by default.
  }

  /**
   * {@inheritdoc}
   */
  public function evaluationDisplay(Answer $answer): array {
    $data = $answer->getData();

    if (
      \array_key_exists('answer', $data) &&
      \is_string($data['answer'])
    ) {
      $answer_renderable = ['#markup' => $data['answer']];
    }
    else {
      $answer_renderable = ['#markup' => $this->t('No implementation')];
    }

    return [
      // Render activity.
      'activity' => $this->entityTypeManager->getViewBuilder('lms_activity')->view($answer->getActivity(), 'activity'),
      // Add answer.
      'answer' => [
        '#type' => 'fieldset',
        '#title' => $this->t('Student answer'),
        'answer' => $answer_renderable,
      ],
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function install(ActivityType $activity_type): void {
    $this->pluginConfigInstaller->install($this->getPluginDefinition(), $activity_type->id());
  }

}
