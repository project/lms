<?php

declare(strict_types=1);

namespace Drupal\lms\Plugin\views\field;

use Drupal\Component\Render\MarkupInterface;
use Drupal\Core\Link;
use Drupal\Core\Session\AccountInterface;
use Drupal\lms\Entity\CourseStatus as CourseStatusEntity;
use Drupal\views\Attribute\ViewsField;
use Drupal\views\Plugin\views\field\FieldPluginBase;
use Drupal\views\ResultRow;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * LMS Course user status field.
 */
#[ViewsField(id: 'lms_student_course_status')]
final class CourseStatus extends FieldPluginBase {

  /**
   * The current user.
   */
  private AccountInterface $currentUser;

  /**
   * Injects services.
   *
   * @param \Symfony\Component\DependencyInjection\ContainerInterface $container
   *   The dependency injection container.
   */
  public function injectServices(ContainerInterface $container): void {
    $this->currentUser = $container->get('current_user');
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    $instance = parent::create($container, $configuration, $plugin_id, $plugin_definition);
    $instance->injectServices($container);
    return $instance;
  }

  /**
   * {@inheritdoc}
   */
  public function render(ResultRow $values): MarkupInterface {
    $status = $this->getValue($values);
    if ($status === NULL) {
      return $this->t('Not started');
    }

    $status_text = CourseStatusEntity::getStatusText($status);
    $status_class = \strtr($status, ' ', '-');

    $course_status = $values->_relationship_entities['course_status'] ?? NULL;
    if ($course_status === NULL) {
      $course_status = $values->_entity;
    }
    if (!$course_status instanceof CourseStatusEntity) {
      $renderable = [
        '#type' => 'html_tag',
        '#tag' => 'p',
        '#attributes' => [
          'class' => [$status_class],
        ],
        'text' => [
          '#markup' => $status_text,
        ],
      ];
      return $this->getRenderer()->render($renderable);
    }

    // Output as link to results.
    $route_parameters = [
      'group' => $course_status->getCourseId(),
    ];
    if ($course_status->getUserId() === (string) $this->currentUser->id()) {
      $route = 'lms.group.self_results';
    }
    else {
      $route = 'lms.group.results';
      $route_parameters['user'] = $course_status->getUserId();
    }
    $renderable = Link::createFromRoute($status_text, $route, $route_parameters, [
      'attributes' => [
        '_target' => 'blank',
        'class' => [$status_class],
      ],
    ])->toRenderable();
    return $this->getRenderer()->render($renderable);
  }

}
