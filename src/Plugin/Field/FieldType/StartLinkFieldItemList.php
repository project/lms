<?php

declare(strict_types=1);

namespace Drupal\lms\Plugin\Field\FieldType;

use Drupal\Core\Cache\CacheableDependencyInterface;
use Drupal\Core\Cache\CacheableDependencyTrait;
use Drupal\Core\Cache\CacheableMetadata;
use Drupal\Core\Field\FieldItemList;
use Drupal\Core\Link;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\Core\TypedData\ComputedItemListTrait;
use Drupal\Core\Url;
use Drupal\lms\Entity\Bundle\Course;
use Drupal\lms\Entity\CourseStatusInterface;
use Drupal\lms\TrainingManager;

/**
 * Field item list class for group's 'start_link' field.
 *
 * @see lms_entity_bundle_field_info()
 */
final class StartLinkFieldItemList extends FieldItemList implements CacheableDependencyInterface {

  use CacheableDependencyTrait;
  use ComputedItemListTrait;

  /**
   * {@inheritdoc}
   *
   * Phpstan doesn't work well with abstract method in a trait.
   *
   * @phpstan-ignore-next-line
   */
  protected function computeValue(): void {
    $group = \Drupal::routeMatch()->getParameter('group');
    if ($group instanceof Course) {
      $renderer = \Drupal::getContainer()->get('renderer');
      $build = $this->build($group);
      $this->list[0] = $this->createItem(0, $renderer->render($build));
    }
  }

  /**
   * Returns the render array build.
   *
   * @param \Drupal\lms\Entity\Bundle\Course $course
   *   The course entity.
   *
   * @return array
   *   Render array.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   * @throws \Drupal\Core\Entity\EntityMalformedException
   */
  private function build(Course $course): array {
    // Allow other modules to conditionally return a link.
    $build = \Drupal::moduleHandler()->invokeAll('lms_course_link', [$course]);
    if (\count($build) !== 0) {
      return $build;
    }

    // @todo This array is inherited while moving the code around but there's no
    //   way to be set as the field is not configurable. Needs a decision.
    $htmlClasses = [];

    $link = $actionInfo = NULL;

    $cacheability = new CacheableMetadata();
    $cacheability->addCacheableDependency($course);

    $cacheContexts = [
      'user.roles:authenticated',
      'route.group',
    ];

    $currentUser = \Drupal::currentUser();

    $cacheability->addCacheTags([
      'group.classes:' . $course->id(),
      TrainingManager::trainingStatusTag($course->id(), (string) $currentUser->id()),
    ]);

    if ($currentUser->isAnonymous()) {
      $url = Url::fromRoute(
        'user.login',
        ['destination' => $course->toUrl()->toString()],
        ['attributes' => ['class' => $htmlClasses + ['join-link']]],
      );
      $link = Link::fromTextAndUrl('Login / Register', $url)->toRenderable();
      $this->setCacheability($cacheability->addCacheContexts($cacheContexts));
      return $this->getBuild(link: $link);
    }

    $takeAccess = $course->takeAccess($currentUser);
    $cacheability->addCacheableDependency($takeAccess);
    if (!$takeAccess->isAllowed()) {
      $classes = $course->getClasses();
      if (\count($classes) === 0) {
        $actionInfo = self::buildActionInfo('no-classes', $this->t('No classes available'));
      }
      $url = Url::fromRoute(
        'entity.group.join',
        ['group' => $course->id()],
        [
          'attributes' => [
            'class' => $htmlClasses + ['join-link'],
            'data-toggle' => 'modal',
            'data-target' => '#join-group-form-overlay',
          ],
        ],
      );
      $link = Link::fromTextAndUrl($this->t('Enroll'), $url)->toRenderable();
    }
    else {
      $trainingManager = \Drupal::getContainer()->get('lms.training_manager');
      $courseStatus = $trainingManager->loadCourseStatus($course, $currentUser, [
        'current' => TRUE,
      ]);
      if ($courseStatus !== NULL) {
        $cacheability->addCacheableDependency($courseStatus);
      }
      $status = $courseStatus?->getStatus();

      $url = Url::fromRoute(
        'lms.course.start',
        ['group' => $course->id()],
        ['attributes' => ['class' => $htmlClasses + ['start-link']]],
      );
      if ($status === NULL) {
        $link = Link::fromTextAndUrl($this->t('Start'), $url)->toRenderable();
      }
      elseif ($status === CourseStatusInterface::STATUS_PROGRESS) {
        $link = Link::fromTextAndUrl($this->t('Continue'), $url)->toRenderable();
      }
      elseif (
        $status === CourseStatusInterface::STATUS_FAILED ||
        $status === CourseStatusInterface::STATUS_PASSED
      ) {
        if ($course->revisitMode()) {
          $link = Link::fromTextAndUrl($this->t('Revisit'), $url)->toRenderable();
        }
        else {
          // @todo Add a confirmation step when restarting.
          $link = Link::fromTextAndUrl($this->t('Restart'), $url)->toRenderable();
        }
      }
      elseif ($status === CourseStatusInterface::STATUS_NEEDS_EVALUATION) {
        $actionInfo = self::buildActionInfo('needs-evaluation', $this->t('Awaits grading'));
      }
    }

    $this->setCacheability($cacheability);
    return $this->getBuild(link: $link, actionInfo: $actionInfo);
  }

  /**
   * Returns the final theme render array.
   *
   * @param array|null $link
   *   The link as render array, if any.
   * @param array|null $actionInfo
   *   The action info as render array, if any.
   *
   * @return array
   *   Render array.
   */
  private function getBuild(?array $link = NULL, ?array $actionInfo = NULL): array {
    return [
      '#theme' => 'lms_start_link',
      '#link' => $link,
      '#action_info' => $actionInfo,
    ];
  }

  /**
   * Builds the themed action info.
   *
   * @param string $class
   *   The CSS class.
   * @param \Drupal\Core\StringTranslation\TranslatableMarkup $info
   *   Message to be displayed to end-user.
   *
   * @return array
   *   Render array.
   */
  public static function buildActionInfo(string $class, TranslatableMarkup $info): array {
    return [
      '#theme' => 'lms_course_action_info',
      '#class' => $class,
      '#info_text' => $info,
    ];
  }

}
