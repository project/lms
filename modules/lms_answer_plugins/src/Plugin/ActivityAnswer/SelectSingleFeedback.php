<?php

declare(strict_types=1);

namespace Drupal\lms_answer_plugins\Plugin\ActivityAnswer;

use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Ajax\InvokeCommand;
use Drupal\Core\Ajax\ReplaceCommand;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\lms\Attribute\ActivityAnswer;
use Drupal\lms\Entity\Answer;
use Drupal\lms_answer_plugins\Plugin\SelectBase;

/**
 * Single-select with feedback activity plugin.
 */
#[ActivityAnswer(
  id: 'select_single_feedback',
  name: new TranslatableMarkup('Single answer select with feedback'),
)]
final class SelectSingleFeedback extends SelectBase {
  protected const ELEMENT_TYPE = 'radios';

  /**
   * {@inheritdoc}
   */
  public function answeringForm(array &$form, FormStateInterface $form_state, Answer $answer): void {
    parent::answeringForm($form, $form_state, $answer);
    $activity = $answer->getActivity();
    $activity_id = $activity->id();

    // Add feedback container with activity ID for one-page lessons.
    $form['answer']['feedback'] = [
      '#type' => 'container',
      '#attributes' => [
        'data-lms-selector' => 'feedback-' . $activity_id,
      ],
      '#weight' => 10,
    ];
    $form['answer']['#attributes']['data-lms-selector'] = 'activity-' . $activity_id;

    // Initially hide Submit button unless revisiting question.
    if (!\array_key_exists('answer', $answer->getData())) {
      $form['actions']['submit']['#access'] = FALSE;
    }

    // Add 'Check Answer' button before Submit button.
    $form['actions']['check'] = [
      '#type' => 'button',
      '#value' => $this->t('Check Answer'),
      '#ajax' => [
        'callback' => [$this, 'getFeedback'],
      ],
      '#weight' => 1,
    ];
    $form['actions']['submit']['#weight'] = 2;
  }

  /**
   * Feedback callback.
   */
  public function getFeedback(array $form, FormStateInterface $form_state): AjaxResponse {
    $response = new AjaxResponse();

    /** @var \Drupal\Core\Entity\ContentEntityFormInterface $form_object */
    $form_object = $form_state->getFormObject();

    /** @var \Drupal\lms\Entity\Answer $answer */
    $answer = $form_object->getEntity();
    $current_answer = $form_state->getValue('answer');
    $answer->setData(['answer' => $current_answer]);
    // Since ActivityAnswerInterface::getScore() returns float, we
    // can check if the given answer is correct by mapping score to
    // percentages - anything equal to or above 0.995 will be 100%.
    $is_correct = $this->getScore($answer) >= 0.995;
    $class = $is_correct ? 'correct-answer' : 'wrong-answer';

    $activity = $answer->getActivity();
    $activity_id = $activity->id();
    $selector_attr = 'data-lms-selector="activity-' . $activity_id . '"';

    // Remove any previous style classes if the answer was changed.
    $response->addCommand(new InvokeCommand(
      '[' . $selector_attr . '] .form-item-answer',
      'removeClass',
      ['correct-answer wrong-answer']
    ));

    // Set style class on selected answer.
    $response->addCommand(new InvokeCommand(
      '[' . $selector_attr . '] .form-item-answer:has([value="' . $current_answer . '"])',
      'addClass',
      [$class]
    ));

    // Make Submit button visible.
    $form['actions']['submit']['#access'] = TRUE;
    $response->addCommand(new ReplaceCommand('.form-actions', $form['actions']));

    // Display feedback.
    $feedback_field = $is_correct ? 'feedback_if_correct' : 'feedback_if_wrong';
    $feedback = $activity->get($feedback_field);
    if (!$feedback->isEmpty()) {
      $form['answer']['feedback']['value'] = [
        '#markup' => $feedback->value,
      ];
    }
    $form['answer']['feedback']['#attributes']['class'] = [$class];
    $response->addCommand(new ReplaceCommand(
      '[data-lms-selector="feedback-' . $activity_id . '"]',
      $form['answer']['feedback']
    ));

    return $response;
  }

}
