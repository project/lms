<?php

declare(strict_types=1);

namespace Drupal\lms_answer_plugins\Plugin\ActivityAnswer;

use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\lms\Attribute\ActivityAnswer;
use Drupal\lms_answer_plugins\Plugin\SelectBase;

/**
 * Long Answer activity plugin.
 */
#[ActivityAnswer(
  id: 'select_multiple',
  name: new TranslatableMarkup('Multiple answer select'),
)]
final class SelectMultiple extends SelectBase {
  protected const ELEMENT_TYPE = 'checkboxes';

}
