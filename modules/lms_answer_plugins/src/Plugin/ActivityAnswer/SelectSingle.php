<?php

declare(strict_types=1);

namespace Drupal\lms_answer_plugins\Plugin\ActivityAnswer;

use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\lms\Attribute\ActivityAnswer;
use Drupal\lms_answer_plugins\Plugin\SelectBase;

/**
 * Long Answer activity plugin.
 */
#[ActivityAnswer(
  id: 'select_single',
  name: new TranslatableMarkup('Single answer select'),
)]
final class SelectSingle extends SelectBase {
  protected const ELEMENT_TYPE = 'radios';

}
