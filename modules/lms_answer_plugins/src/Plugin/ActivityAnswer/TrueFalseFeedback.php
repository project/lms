<?php

declare(strict_types=1);

namespace Drupal\lms_answer_plugins\Plugin\ActivityAnswer;

use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Ajax\ReplaceCommand;
use Drupal\Core\Ajax\InvokeCommand;
use Drupal\lms\Attribute\ActivityAnswer;
use Drupal\lms\Entity\Answer;
use Drupal\lms_answer_plugins\Plugin\TrueFalseBase;

/**
 * True/False with feedback activity plugin.
 */
#[ActivityAnswer(
  id: 'true_false_feedback',
  name: new TranslatableMarkup('True / false with feedback'),
)]
final class TrueFalseFeedback extends TrueFalseBase {

  /**
   * {@inheritdoc}
   */
  public function answeringForm(array &$form, FormStateInterface $form_state, Answer $answer): void {
    parent::answeringForm($form, $form_state, $answer);
    $activity = $answer->getActivity();

    // Add feedback container.
    $form['answer']['feedback'] = [
      '#type' => 'container',
      '#attributes' => ['id' => 'feedback-wrapper'],
      '#weight' => 10,
    ];

    // Check if an answer has been selected.
    $current_answer = $form_state->getValue('answer');
    if ($current_answer !== NULL) {
      $answer->setData(['answer' => $current_answer]);
      $is_correct = $this->getScore($answer) > 0;
      $class = $this->getAnswerClass($is_correct);

      // Display answer feedback if available.
      $feedback_field = $is_correct ? 'feedback_if_correct' : 'feedback_if_wrong';
      $feedback = $activity->get($feedback_field);
      if (!$feedback->isEmpty()) {
        $form['answer']['feedback']['value'] = [
          '#markup' => $feedback->value,
        ];
      }

      // Add style classes to feedback and current answer.
      $form['answer']['feedback']['#attributes']['class'] = [$class];
      $form['answer'][$current_answer]['#attributes']['class'][] = $class;

      // Store answer so it's available for Feedback callback.
      $form_state->set('answer_result', [
        'value' => $current_answer,
        'correct' => $is_correct,
      ]);

      // Only show submit button after an answer is selected.
      $form['actions']['submit']['#access'] = TRUE;
    }
    else {
      $form['actions']['submit']['#access'] = FALSE;
    }

    // Add 'Check Answer' button before Submit button.
    $form['actions']['check'] = [
      '#type' => 'submit',
      '#value' => $this->t('Check Answer'),
      '#submit' => [[$this, 'checkAnswer']],
      '#ajax' => [
        'callback' => [$this, 'getFeedback'],
        'wrapper' => 'feedback-wrapper',
      ],
      '#weight' => 1,
    ];
    $form['actions']['submit']['#weight'] = 2;

  }

  /**
   * Submit handler for checking the answer.
   */
  public function checkAnswer(array &$form, FormStateInterface $form_state): void {
    $form_state->setRebuild(TRUE);
  }

  /**
   * Feedback callback.
   */
  public function getFeedback(array $form, FormStateInterface $form_state): AjaxResponse {
    $response = new AjaxResponse();

    $answer_result = $form_state->get('answer_result');
    if ($answer_result === NULL) {
      return $response;
    }

    // Remove any previous style classes if the answer was changed.
    $response->addCommand(new InvokeCommand(
      '#edit-answer-0, #edit-answer-1',
      'removeClass',
      ['correct-answer wrong-answer']
    ));

    // Set style classes on current answer.
    $response->addCommand(new InvokeCommand(
      '#edit-answer-' . $answer_result['value'],
      'addClass',
      [$this->getAnswerClass($answer_result['correct'])]
    ));

    // Make Submit button visible.
    $form['actions']['submit']['#access'] = TRUE;
    $response->addCommand(new ReplaceCommand('.form-actions', $form['actions']));

    // Display feedback.
    $response->addCommand(new ReplaceCommand('#feedback-wrapper', $form['answer']['feedback']));

    return $response;
  }

  /**
   * Provide style classes for answers.
   */
  private function getAnswerClass(bool $is_correct): string {
    return $is_correct ? 'correct-answer' : 'wrong-answer';
  }

}
