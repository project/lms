<?php

declare(strict_types=1);

namespace Drupal\lms_answer_plugins\Plugin;

use Drupal\Core\Form\FormStateInterface;
use Drupal\lms\Entity\Answer;
use Drupal\lms\Plugin\ActivityAnswerBase;
use Drupal\lms_answer_plugins\Plugin\Field\FieldType\LmsAnswer;

/**
 * Base class for select activity plugins.
 */
abstract class SelectBase extends ActivityAnswerBase {

  protected const ELEMENT_TYPE = NULL;

  /**
   * {@inheritdoc}
   */
  public function getScore(Answer $answer): float {
    $answers = $answer->getActivity()->get('answers');
    $data = $answer->getData()['answer'];
    if (!\is_array($data)) {
      $data = [$data];
    }

    $data = \array_filter($data, static fn($item) => $item !== 0);
    $score = 0;
    $max_score = 0;
    foreach ($answers as $delta => $answer_item) {
      if (\in_array((string) $delta, $data, TRUE)) {
        $answer = TRUE;
      }
      else {
        $answer = FALSE;
      }

      \assert($answer_item instanceof LmsAnswer);
      if ($answer_item->isCorrect()) {
        $max_score++;
        // Checking the correct answer +1.
        if ($answer) {
          $score++;
        }
        // Not checking the correct answer 0.
      }
      // Checking an incorrect answer -1.
      elseif ($answer) {
        $score--;
      }
    }

    // Result.
    $result = 0;

    // No correct answers - only 0 works.
    if ($max_score === 0) {
      if ($score === 0) {
        $result = 1;
      }
    }
    // More or equal incorrect checked than correct - 0.
    // Calculate fraction otherwise.
    elseif ($score > 0) {
      $result = $score / $max_score;
    }

    return $result;
  }

  /**
   * {@inheritdoc}
   */
  public function answeringForm(array &$form, FormStateInterface $form_state, Answer $answer): void {
    $activity = $answer->getActivity();
    $data = $answer->getData();
    $options = [];
    foreach ($activity->get('answers') as $delta => $answer_item) {
      $options[$delta] = $answer_item->get('answer')->getValue();
    }

    $form['answer'] = [
      '#title' => $this->t('Your answer'),
      '#type' => static::ELEMENT_TYPE,
      '#options' => $options,
      '#default_value' => $data['answer'] ?? [],
    ];
  }

}
