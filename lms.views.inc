<?php

/**
 * @file
 * Course views custom implementation.
 */

declare(strict_types=1);

/**
 * Implements hook_views_data_alter().
 */
function lms_views_data_alter(array &$data): void {
  $data['groups_field_data']['lms_course_take_link'] = [
    'title' => t('Take course'),
    'field' => [
      'title' => t('Course take link'),
      'help' => t('Course take link'),
      'id' => 'lms_course_take_link',
    ],
  ];

  $data['group_relationship_field_data']['classes_filter'] = [
    'title' => t('Filter relationships by child class'),
    'filter' => [
      'title' => t('Is content of selected classes'),
      'real field' => 'gid',
      'id' => 'lms_parent_class',
    ],
    'argument' => [
      'title' => t('Group relationships of Course classes'),
      'id' => 'course_classes_relationships',
      'real field' => 'gid',
    ],
  ];

  $data['group_relationship_field_data']['course_status'] = [
    'title' => t('Course status'),
    'help' => t('Relates memberships to the current Course Status entity. Requires Course (lms_course group bundle) context.'),
    'relationship' => [
      'label' => t('Course status'),
      'group' => t('LMS'),
      'real field' => 'gid',
      'base' => 'lms_course_status',
      'id' => 'class_member_to_course_status',
    ],
  ];

}
