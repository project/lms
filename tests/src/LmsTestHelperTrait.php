<?php

declare(strict_types=1);

namespace Drupal\Tests\lms;

use Behat\Mink\Element\NodeElement;
use Drupal\Component\Render\FormattableMarkup;
use Drupal\Component\Serialization\Yaml;
use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Logger\RfcLogLevel;
use Drupal\Core\Render\Markup;
use Drupal\Core\Site\Settings;
use Drupal\Core\Url;
use Drupal\FunctionalJavascriptTests\JSWebAssert;
use Drupal\lms\Entity\CourseStatusInterface;
use Drupal\lms\Entity\LessonStatusInterface;
use Drush\TestTraits\DrushTestTrait;

/**
 * Contains methods useful in tests.
 */
trait LmsTestHelperTrait {

  use DrushTestTrait;

  /**
   * Field name - field type mapping.
   */
  private const TYPES_MAPPING = [
    'bool_expected' => 'radios',
    'mandatory' => 'checkbox',
    'correct' => 'checkbox',
    'backwards_navigation' => 'checkbox',
    'auto_repeat_failed' => 'checkbox',

    // Default mapping.
    'radios' => 'radios',
    'checkbox' => 'checkbox',
    'select' => 'select',
    'input' => 'input',
  ];

  /**
   * Answer data.
   */
  private array $answers = [];

  /**
   * Dev use only - screenshot delta.
   */
  private int $screenShotDelta = 0;

  /**
   * Get existing activity - answer plugin definitions.
   */
  private function getActivityAnswerPluginDefinitions(): array {
    return $this->container->get('plugin.manager.activity_answer')->getDefinitions();
  }

  /**
   * Helper method to set LMS entity reference values.
   */
  private function setLmsReferenceField(string $field_name, array $values): void {
    if ($field_name === 'activities') {
      $entity_type = 'lms_activity';
      $bag = $this->activityData;
    }
    elseif ($field_name === 'lessons') {
      $entity_type = 'lms_lesson';
      $bag = $this->lessonData;
    }
    else {
      throw new \InvalidArgumentException(\sprintf("Unsupported field: %s", $field_name));
    }
    $modal_view_selector = \sprintf('[role="dialog"].modal-view-%s-selection', $field_name);

    $assert_session = $this->assertSession();
    \assert($assert_session instanceof JSWebAssert);

    foreach ($values as $item) {
      $this->getSession()->getPage()->pressButton(\sprintf('Reference %s', $field_name));
      $modal = $assert_session->waitForElementVisible('css', $modal_view_selector);
      $entity = $this->getEntityByProperties($entity_type, [
        'name' => $this->getItemByUuid($item['target_uuid'], $bag, 'name'),
      ]);
      $selector = \sprintf('[value="%s"]', \implode(':', [
        $entity->getEntityTypeId(),
        $entity->bundle(),
        $entity->id(),
      ]));
      $modal->find('css', $selector)->check();

      $modal->find('css', '.ui-dialog-buttonset > .lms-add-references')->click();
      $assert_session->waitForElementRemoved('css', $modal_view_selector);

      // Open parameters modal.
      $selector = \sprintf('[data-drupal-selector="edit-%s-table-%d-actions-edit-parameters"]', $field_name, $entity->id());
      $assert_session->waitForElementVisible('css', $selector)->click();
      $selector = \sprintf('[role="dialog"].modal-entity-%s', \strtr($entity_type, '_', '-'));
      $modal = $assert_session->waitForElementVisible('css', $selector);
      // Set parameter values.
      foreach ($item as $property => $value) {
        if ($property === 'target_uuid') {
          continue;
        }
        // Warning: it may potentially happen that there is more than one
        // field with the same name on the page.
        $this->setFormElementValue($property, $property, $value);
      }

      // Submit and wait for the modal to close.
      $modal->find('css', 'button.form-submit')->click();
      $assert_session->waitForElementRemoved('css', $selector);
    }
  }

  /**
   * Returns test data property for a given UUID.
   */
  private function getItemByUuid(string $uuid, array $bag, ?string $property = NULL): array|string {
    foreach ($bag as $item) {
      if ($item['uuid'] === $uuid) {
        return $property === NULL ? $item : $item['values'][$property];
      }
    }

    throw new \InvalidArgumentException(\sprintf("UUID %s doesn't exist in test data.", $uuid));
  }

  /**
   * Helper method to filter entity data by owner user UUID.
   */
  private function filterByOwnerUuid(array $filterable, string $uuid): array {
    return \array_filter($filterable, static fn ($item) => $item['owner_uuid'] === $uuid);
  }

  /**
   * Dev helper method to create a screenshot quickly.
   */
  private function screenShot(): void {
    $screenshot_dir = \DRUPAL_ROOT . '/../test_screenshots';
    if (!\is_dir($screenshot_dir)) {
      return;
    }
    if ($this->screenShotDelta === 0) {
      for ($i = 0;; $i++) {
        $filename = $screenshot_dir . '/test_' . $i . '.jpeg';
        if (!\file_exists($filename)) {
          break;
        }
        \unlink($filename);
      }
    }
    $this->createScreenshot($screenshot_dir . '/test_' . $this->screenShotDelta . '.jpeg');
    $this->screenShotDelta++;
  }

  /**
   * Import test config.
   *
   * Not used currently, leaving not to reinvent if needed.
   */
  private function importTestConfig(string $directory = ''): void {
    if ($directory === '') {
      $directory = __DIR__ . '/../config';
    }
    $active = $this->container->get('config.storage');
    $sync = $this->container->get('config.storage.sync');
    $this->copyConfig($active, $sync);
    $file_system = $this->container->get('file_system');
    $config_sync_dir = Settings::get('config_sync_directory');
    // Path in the project dir: /tests/config.
    foreach ($file_system->scanDirectory($directory, '/.*\.yml/') as $file) {
      $file_system->copy($file->uri, $config_sync_dir);
    }
    $this->configImporter()->import();
  }

  /**
   * Get entity ID by properties.
   */
  private function getEntityIdByProperties(string $entity_type_id, array $properties): string {
    $query = $this->container->get('entity_type.manager')->getStorage($entity_type_id)->getQuery();
    $query->accessCheck(FALSE);
    foreach ($properties as $property => $value) {
      $query->condition($property, $value);
    }
    $results = $query->execute();
    if (\count($results) === 0) {
      throw new \Exception(\sprintf('Unable to find entity type %s with %s', $entity_type_id, print_r($properties, TRUE)));
    }
    return \reset($results);
  }

  /**
   * Get entity by properties.
   */
  private function getEntityByProperties(string $entity_type_id, array $properties): ContentEntityInterface {
    /** @var \Drupal\Core\Entity\ContentEntityInterface */
    return $this->container->get('entity_type.manager')->getStorage($entity_type_id)->load($this->getEntityIdByProperties($entity_type_id, $properties));
  }

  /**
   * Set entity form field value.
   */
  private function setEntityFormField(string $field, mixed $value, int $delta = 0, array $path = ['value'], bool $multiple = FALSE): void {
    // Multi value fields.
    if (\is_array($value)) {
      $count = \count($value);
      $current = 0;
      foreach ($value as $internal_delta => $item) {
        foreach ($item as $property => $property_value) {
          $this->setEntityFormField($field, $property_value, $internal_delta, [$property], $multiple);
        }
        $current++;
        if ($current < $count) {
          $this->getSession()->getPage()->pressButton('Add another item');
          $assert_session = $this->assertSession();
          \assert($assert_session instanceof JSWebAssert);
          $assert_session->waitForElementVisible('css', '[name="' . $field . '_' . $current . '_remove_button"]');
        }
      }

      return;
    }

    // Handle different types.
    $type = self::TYPES_MAPPING[$field] ?? 'input';
    if ($type === 'radios') {
      $selector = \strtr('edit-' . $field, '_', '-');
    }
    else {
      $path = [$delta, ...$path];

      // Single value checkboxes don't have deltas in their path.
      if ($type === 'checkbox' && !$multiple) {
        \array_shift($path);
      }

      $selector = $field;
      foreach ($path as $path_component) {
        $selector .= '[' . $path_component . ']';
      }
    }

    $this->setFormElementValue($type, $selector, $value);
  }

  /**
   * Helper method to set any form field.
   */
  private function setFormElementValue(string $field, string $selector, mixed $value): void {
    $type = self::TYPES_MAPPING[$field] ?? 'input';

    if ($type === 'checkbox') {
      if ($value === TRUE) {
        $this->getSession()->getPage()->checkField($selector);
      }
      else {
        $this->getSession()->getPage()->uncheckField($selector);
      }
    }
    elseif ($type === 'radios') {
      if ($value === TRUE) {
        $value = '1';
      }
      if ($value === FALSE) {
        $value = '0';
      }
      $selector .= '-' . $value;
      $this->getSession()->getPage()->selectFieldOption($selector, $value);
    }
    elseif ($type === 'select') {
      $this->getSession()->getPage()->selectFieldOption($selector, $value);
    }
    else {
      $this->getSession()->getPage()->fillField($selector, (string) $value);
    }
  }

  /**
   * Answer an activity.
   */
  private function answerActivity(array $activity_item, int $max_score, string $student_id, string $course_id): void {
    $page = $this->getSession()->getPage();
    $uuid = $activity_item['uuid'];

    // Long answer.
    if ($activity_item['type'] === 'free_text') {
      $answer = 'Answer to ' . $activity_item['values']['name'] . ' ' . $uuid;
      $this->answers[$course_id][$student_id][$uuid] = [
        'answer' => $answer,
        'evaluated' => FALSE,
      ];
      $this->setFormElementValue('input', 'edit-answer-0', $answer);
    }

    // True / false question.
    elseif ($activity_item['type'] === 'true_false') {
      $answer = (string) \mt_rand(0, 1);
      $this->answers[$course_id][$student_id][$uuid] = [
        'answer' => $answer,
        'score' => $activity_item['values']['bool_expected'] === (bool) $answer ? $max_score : 0,
        'evaluated' => TRUE,
      ];
      $this->setFormElementValue('radios', 'edit-answer', $answer);
    }

    // No answer activity.
    elseif ($activity_item['type'] === 'no_answer') {
      $this->answers[$course_id][$student_id][$uuid] = [
        'answer' => NULL,
        'score' => $max_score,
        'evaluated' => TRUE,
      ];
    }

    elseif ($activity_item['type'] === 'select_single') {
      $answers_count = \count($activity_item['values']['answers']);
      $answer = (string) \mt_rand(0, $answers_count - 1);
      $this->answers[$course_id][$student_id][$uuid] = [
        'answer' => $answer,
        'score' => $activity_item['values']['answers'][$answer]['correct'] ? $max_score : 0,
        'evaluated' => TRUE,
      ];
      $this->setFormElementValue('radios', 'edit-answer', $answer);
    }

    elseif ($activity_item['type'] === 'select_multiple') {
      $max_answers_count = \count($activity_item['values']['answers']);
      $possible_answers = [];
      for ($i = 0; $i < $max_answers_count; $i++) {
        $possible_answers[$i] = $i;
      }
      $given_answers_count = \mt_rand(0, $max_answers_count);
      // @todo array_rand is not very flexible, find a more optimal way.
      if ($given_answers_count === 0) {
        $answer = [];
      }
      elseif ($given_answers_count === 1) {
        $answer = [\array_rand($possible_answers, $given_answers_count)];
      }
      else {
        $answer = \array_rand($possible_answers, $given_answers_count);
      }

      $points = 0;
      $max_points = 0;
      foreach ($possible_answers as $delta) {
        $correct = $activity_item['values']['answers'][$delta]['correct'];
        if ($correct) {
          $max_points++;
        }
        if (\in_array($delta, $answer, TRUE)) {
          $value = TRUE;
          if ($correct) {
            $points++;
          }
          else {
            $points--;
          }
        }
        else {
          $value = FALSE;
        }
        $this->setFormElementValue('checkbox', 'edit-answer-' . $delta, $value);
      }

      if ($max_points === 0) {
        if ($points === 0) {
          $score = 1;
        }
        else {
          $score = 0;
        }
      }
      else {
        if ($points < 0) {
          $points = 0;
        }
        $score = $points / $max_points;
      }

      $this->answers[$course_id][$student_id][$uuid] = [
        'answer' => $answer,
        'score' => \round($max_score * $score),
        'evaluated' => TRUE,
      ];
    }

    $page->pressButton('edit-submit');
  }

  /**
   * Get answer.
   */
  private function getAnswerData(string $activity_uuid, string $student_id, string $course_id): array {
    return $this->answers[$course_id][$student_id][$activity_uuid];
  }

  /**
   * Used in both answer evaluation methods.
   */
  private function getAnswerEvaluateParameters(array $activity_item, array $lesson_item, string $student_id, string $course_id): array {
    $course_status_id = $this->getEntityIdByProperties('lms_course_status', [
      'uid' => $student_id,
      'gid' => $course_id,
    ]);
    $lesson = $this->getEntityByProperties('lms_lesson', [
      'name' => $lesson_item['values']['name'],
    ]);
    $lesson_status_id = $this->getEntityIdByProperties('lms_lesson_status', [
      'course_status' => $course_status_id,
      'lesson' => $lesson->id(),
    ]);

    $activity = $this->getEntityByProperties('lms_activity', [
      'name' => $activity_item['values']['name'],
    ]);

    $answer_id = $this->getEntityIdByProperties('lms_answer', [
      'user_id' => $student_id,
      'lesson_status' => $lesson_status_id,
      'activity' => $activity->id(),
    ]);
    return [
      $lesson,
      $activity,
      $answer_id,
    ];
  }

  /**
   * Evaluate answer using results page modal form.
   */
  private function evaluateAnswerModal(array $activity_item, array $lesson_item, string $student_id, string $course_id): void {
    $assert_session = $this->assertSession();
    \assert($assert_session instanceof JSWebAssert);
    // Drupal CI adds /web in front of URIs and this assert fails.
    if (FALSE) {
      $url = Url::fromRoute('lms.group.results', [
        'group' => $course_id,
        'user' => $student_id,
      ])->toString();
      $assert_session->addressEquals($url);
    }

    [$lesson, $activity, $answer_id] = $this->getAnswerEvaluateParameters($activity_item, $lesson_item, $student_id, $course_id);

    $evaluate_uri = Url::fromRoute('lms.answer.evaluate', [
      'lms_answer' => $answer_id,
      'js' => 'nojs',
    ])->toString();
    $this->getSession()->getPage()->find('css', '[href="' . $evaluate_uri . '"]')->click();

    $modal = $assert_session->waitForElementVisible('css', '[role="dialog"]');
    $answer_data = &$this->answers[$course_id][$student_id][$activity_item['uuid']];
    $this->assertElementTextContains($modal, $answer_data['answer'], \sprintf('Answer %s not found on evaluation modal.', $answer_data['answer']));

    $max_score = $this->container->get('lms.training_manager')->getActivityMaxScore($lesson, $activity);
    $answer_data['score'] = (string) \mt_rand(0, $max_score);
    $modal->fillField('score', $answer_data['score']);

    // Click the right button (there are 3, of which one is hidden).
    $modal->find('css', 'button.form-submit')->click();
    // Modal should be closed.
    $assert_session->waitForElementRemoved('css', '[role="dialog"]');
  }

  /**
   * Evaluate answer no JS.
   */
  private function evaluateAnswerNoJs(array $activity_item, array $lesson_item, string $student_id, string $course_id): void {
    [$lesson, $activity, $answer_id] = $this->getAnswerEvaluateParameters($activity_item, $lesson_item, $student_id, $course_id);

    $this->drupalGet(Url::fromRoute('lms.answer.evaluate', [
      'lms_answer' => $answer_id,
      'js' => 'nojs',
    ]));

    $answer_data = &$this->answers[$course_id][$student_id][$activity_item['uuid']];

    $this->assertSession()->pageTextContains($answer_data['answer']);

    $page = $this->getSession()->getPage();
    $answer_data = &$this->answers[$course_id][$student_id][$activity_item['uuid']];
    $max_score = $this->container->get('lms.training_manager')->getActivityMaxScore($lesson, $activity);
    $answer_data['score'] = (string) \mt_rand(0, $max_score);
    $page->fillField('score', $answer_data['score']);
    $page->pressButton('Evaluate');
  }

  /**
   * Calculate course score.
   *
   * NOTE: Will not calculate properly with randomization enabled or
   * for not evaluated course attempts use only if course is finished
   * and evaluated.
   *
   * @return array
   *   Array containing pass / fail boolean and score in percents.
   */
  private function calculateCourseResult(array $course_item, string $student_id): array {
    $course_id = $this->getEntityIdByProperties('group', [
      'label' => $course_item['values']['label'],
    ]);
    $answer_data = $this->answers[$course_id][$student_id];

    $passed = TRUE;
    $max_score = 0;
    $score_weighted_sum = 0;
    foreach ($course_item['lessons'] as $course_lesson_item) {
      $lesson_item = $this->getItemByUuid($course_lesson_item['target_uuid'], $this->lessonData);
      $lesson_max_score = 0;
      $lesson_score = 0;
      foreach ($lesson_item['activities'] as $lesson_activity_item) {
        $lesson_max_score += $lesson_activity_item['max_score'];
        $lesson_score += $answer_data[$lesson_activity_item['target_uuid']]['score'];
      }

      // Lesson score.
      if ($lesson_max_score === 0) {
        $lesson_score = 100;
      }
      else {
        $lesson_score = (int) \round($lesson_score / $lesson_max_score * 100);
      }

      $max_score += $lesson_max_score;
      $score_weighted_sum += $lesson_score * $lesson_max_score;

      if ($lesson_score < $course_lesson_item['required_score']) {
        $passed = FALSE;
      }
    }

    return [
      $passed,
      (int) \round($score_weighted_sum / $max_score),
    ];
  }

  /**
   * Code saver.
   */
  private function assertElementTextContains(NodeElement $element, string $text, string $message): void {
    $element_text = $element->getText();
    $this::assertTrue(\strpos($element_text, $text) !== FALSE, $message);
  }

  /**
   * Scoring all unevaluated answers and recalculate statuses.
   */
  private function evaluateCourse(string $course_id, string $student_id): void {
    $course_status = $this->getEntityByProperties('lms_course_status', [
      'gid' => $course_id,
      'uid' => $student_id,
    ]);
    \assert($course_status instanceof CourseStatusInterface);
    $training_manager = $this->container->get('lms.training_manager');
    foreach ($this->container->get('entity_type.manager')->getStorage('lms_lesson_status')->loadByProperties([
      'course_status' => $course_status->id(),
    ]) as $lesson_status) {
      \assert($lesson_status instanceof LessonStatusInterface);
      /** @var \Drupal\lms\Entity\AnswerInterface */
      foreach ($this->container->get('entity_type.manager')->getStorage('lms_answer')->loadByProperties([
        'lesson_status' => $lesson_status->id(),
      ]) as $answer) {
        if (!$answer->isEvaluated()) {
          $max_score = $training_manager->getActivityMaxScore($lesson_status->getLesson(), $answer->getActivity());
          $answer
            ->setScore($max_score)
            ->setEvaluated(TRUE)
            ->save();
        }
      }
      $training_manager->updateLessonStatus($lesson_status);
    }
    $training_manager->updateCourseStatus($course_status);
  }

  /**
   * Set source data.
   */
  private function setSourceData(): void {
    foreach ([
      'activityData' => __DIR__ . '/../data/activities.yml',
      'lessonData' => __DIR__ . '/../data/lessons.yml',
      'courseData' => __DIR__ . '/../data/courses.yml',
      'userData' => __DIR__ . '/../data/users.yml',
    ] as $property => $source_file) {
      if (\property_exists($this, $property)) {
        $this->{$property} = Yaml::decode(\file_get_contents($source_file));
      }
    }
  }

  /**
   * Create test content using Drush .
   */
  private function createTestContent(string $module = ''): void {
    if ($module === '') {
      $this->drush('lms-ctt', [], ['simple-passwords' => TRUE]);
    }
    else {
      $this->drush('lms-ctt', [], ['simple-passwords' => TRUE, 'module' => $module]);
    }

    // Set passwords for login on users.
    if (
      !\property_exists($this, 'userData') ||
      !\property_exists($this, 'users')
    ) {
      return;
    }
    $users = $this->container->get('entity_type.manager')->getStorage('user')->loadMultiple();
    foreach ($users as $user) {
      foreach ($this->userData as $name => $item) {
        if ($item['uuid'] === $user->uuid()) {
          $user->pass_raw = '123456';
          $user->passRaw = $user->pass_raw;
          $this->users[$name] = $user;
          continue;
        }
      }
    }

  }

  /**
   * Watchdog cleanness test.
   */
  private function watchdogTest(): void {
    $query = $this->container->get('database')->select('watchdog', 'w');
    $results = $query
      ->condition('severity', RfcLogLevel::NOTICE, '<=')
      ->condition('type', ['php', 'modal_form'], 'IN')
      ->fields('w', [
        'type',
        'message',
        'variables',
        'severity',
        'location',
      ])
      ->execute()
      ->fetchAll(\PDO::FETCH_ASSOC);

    $messages = [];
    foreach ($results as $result) {
      $variables = \unserialize($result['variables'], ['allowed_classes' => [Markup::class]]);
      $message = new FormattableMarkup($result['message'], $variables);
      $messages[] = \sprintf('Severity: %d, type: %s, location: %s, message: %s',
        $result['severity'],
        $result['type'],
        $result['location'],
        \strip_tags((string) $message)
      );
    }

    self::assertEmpty($messages, \implode(\PHP_EOL, $messages));
  }

}
