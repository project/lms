<?php

declare(strict_types=1);

namespace Drupal\Tests\lms\FunctionalJavascript;

use Drupal\Component\Serialization\Yaml;
use Drupal\Core\Url;
use Drupal\FunctionalJavascriptTests\JSWebAssert;
use Drupal\FunctionalJavascriptTests\WebDriverTestBase;
use Drupal\Tests\lms\LmsTestHelperTrait;
use Drupal\lms\Entity\CourseStatus;
use Drupal\lms\Hook\EntityHooks;

/**
 * General javascript test of LMS basic features.
 *
 * @group lms
 */
final class GeneralLmsTest extends WebDriverTestBase {

  use LmsTestHelperTrait;

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'dblog',
    'block',
    'page_cache',
    'dynamic_page_cache',
    'lms',
    'lms_answer_plugins',
  ];

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'claro';

  /**
   * Test user data.
   */
  private array $userData;

  /**
   * Test activity data.
   */
  private array $activityData;

  /**
   * Test lesson data.
   */
  private array $lessonData;

  /**
   * Test Course data.
   */
  private array $courseData;

  /**
   * Test user accounts.
   */
  private array $users = [];

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();

    // Set source data.
    $this->setSourceData();

    // Import test config.
    $this->importTestConfig();

    // Create test users.
    foreach ($this->userData as $name => $values) {
      $this->users[$name] = $this->drupalCreateUser([], NULL, FALSE, $values);
    }
  }

  /**
   * Test runner.
   *
   * To increase performance and avoid recreating test environment on every
   * run, execute all tests from one method.
   */
  public function testLms(): void {
    // Test admin UI by setting up LMS and creating some test content.
    $this->adminTest();

    // Test teacher UI by creating teacher content.
    $this->teacherCourseCreationTest();

    // Test student - side functionality on the first course.
    $this->course1StudentTest();

    // Test course evaluation and results.
    $this->course1EvaluationTest();

    // Test student - side functionality on the second course.
    $this->course2StudentTest();

    // Test exam functionality.
    $this->examTest();

    // Test modals in Course creation logic.
    $this->lmsReferenceTableTest();

    // Data integrity checking tests.
    $this->testDataIntegrityChecks();

    // Check site logs for any alarming messages as JS tests don't report
    // those if happened outside of the test runner.
    $this->watchdogTest();
  }

  /**
   * Admin user test.
   */
  private function adminTest(): void {
    $page = $this->getSession()->getPage();
    $assert_session = $this->assertSession();

    $this->drupalLogin($this->users['admin']);
    $uuid = $this->users['admin']->get('uuid')->value;

    // Activity type creation test.
    $this->drupalGet(Url::fromRoute('entity.lms_activity_type.collection'));

    $definitions = $this->getActivityAnswerPluginDefinitions();
    foreach ($definitions as $definition) {
      $page->clickLink('Add activity type');
      $this->setFormElementValue('input', 'name', $definition['name']);
      $page->pressButton('Edit');
      $this->setFormElementValue('input', 'id', $definition['id']);
      $this->setFormElementValue('select', 'pluginId', $definition['id']);
      $page->pressButton('edit-submit');
    }
    foreach ($definitions as $definition) {
      $assert_session->pageTextContains($definition['name']);
    }

    // Activities creation test (admin).
    $this->drupalGet(Url::fromRoute('entity.lms_activity.collection'));
    $assert_session->linkExists('Add activity');
    foreach ($this->filterByOwnerUuid($this->activityData, $uuid) as $item) {
      $this->drupalGet(Url::fromRoute('entity.lms_activity.add_form', [
        'lms_activity_type' => $item['type'],
      ]));
      foreach ($item['values'] as $field => $value) {
        $this->setEntityFormField($field, $value);
      }
      $page->pressButton('edit-submit');
    }

    $this->drupalGet(Url::fromRoute('entity.lms_activity.collection'));
    foreach ($this->filterByOwnerUuid($this->activityData, $uuid) as $item) {
      $assert_session->pageTextContains($item['values']['name']);
    }

  }

  /**
   * Teacher test.
   */
  private function teacherCourseCreationTest(): void {
    $page = $this->getSession()->getPage();
    $assert_session = $this->assertSession();

    $uuid = $this->users['teacher']->get('uuid')->value;
    $admin_uuid = $this->users['admin']->get('uuid')->value;

    $this->drupalLogin($this->users['teacher']);

    // Activities creation test.
    $this->drupalGet(Url::fromRoute('entity.lms_activity.collection'));
    $this::assertTrue($page->hasLink('Add activity'));
    foreach ($this->filterByOwnerUuid($this->activityData, $uuid) as $item) {
      $this->drupalGet(Url::fromRoute('entity.lms_activity.add_form', [
        'lms_activity_type' => $item['type'],
      ]));
      foreach ($item['values'] as $field => $value) {
        $this->setEntityFormField($field, $value);
      }
      $page->pressButton('edit-submit');
    }

    $this->drupalGet(Url::fromRoute('entity.lms_activity.collection'));
    foreach ($this->filterByOwnerUuid($this->activityData, $uuid) as $item) {
      $assert_session->pageTextContains($item['values']['name']);
    }

    // Test access.
    foreach ($this->filterByOwnerUuid($this->activityData, $admin_uuid) as $item) {
      $assert_session->pageTextNotContains($item['values']['name']);
    }
    foreach ($this->filterByOwnerUuid($this->activityData, $admin_uuid) as $item) {
      $activity_id = $this->getEntityIdByProperties('lms_activity', ['name' => $item['values']['name']]);
      $this->drupalGet(Url::fromRoute('entity.lms_activity.edit_form', [
        'lms_activity' => $activity_id,
      ]));
      $assert_session->pageTextContains('Access denied');
      $assert_session->pageTextNotContains($item['values']['name']);
    }

    // Create lessons.
    $this->drupalGet(Url::fromRoute('entity.lms_lesson.collection'));
    $this::assertTrue($page->hasLink('Add lesson'));

    foreach ($this->filterByOwnerUuid($this->lessonData, $uuid) as $item) {
      $this->drupalGet(Url::fromRoute('entity.lms_lesson.add_form', [
        'lms_lesson_type' => 'lesson',
      ]));
      foreach ($item['values'] as $field => $value) {
        $this->setEntityFormField($field, $value);
      }
      $this->setLmsReferenceField('activities', $item['activities']);
      $page->pressButton('edit-submit');
    }
    $this->drupalGet(Url::fromRoute('entity.lms_lesson.collection'));

    foreach ($this->filterByOwnerUuid($this->lessonData, $uuid) as $item) {
      $assert_session->pageTextContains($item['values']['name']);
    }

    // Create courses.
    $this->drupalGet(Url::fromRoute('entity.group.collection'));
    $this::assertTrue($page->hasLink('Add group'));
    foreach ($this->filterByOwnerUuid($this->courseData, $uuid) as $item) {
      $this->drupalGet(Url::fromRoute('entity.group.add_form', [
        'group_type' => 'lms_course',
      ]));
      foreach ($item['values'] as $field => $value) {
        $this->setEntityFormField($field, $value);
      }
      $this->setLmsReferenceField('lessons', $item['lessons']);
      $page->pressButton('edit-submit');
    }

    $this->drupalGet(Url::fromRoute('entity.group.collection'));
    foreach ($this->filterByOwnerUuid($this->courseData, $uuid) as $item) {
      $assert_session->pageTextContains($item['values']['label']);
      /** @var \Drupal\lms\Entity\Bundle\Course */
      $course = $this->getEntityByProperties('group', [
        'label' => $item['values']['label'],
      ]);
      $assert_session->pageTextContains(EntityHooks::generateDefaultClassName($course));
    }

  }

  /**
   * Course one - test answering questions and navigation.
   */
  private function course1StudentTest(): void {
    $page = $this->getSession()->getPage();
    $assert_session = $this->assertSession();

    $this->drupalLogin($this->users['student']);

    $course_id = $this->getEntityIdByProperties('group', ['label' => $this->courseData[1]['values']['label']]);
    $this->drupalGet(Url::fromRoute('entity.group.canonical', [
      'group' => $course_id,
    ]));
    $page->clickLink('Enroll');
    $page->pressButton('edit-submit');
    $page->clickLink('Start');

    foreach ($this->courseData[1]['lessons'] as $lesson_delta => $course_lesson_item) {
      $lesson_item = $this->getItemByUuid($course_lesson_item['target_uuid'], $this->lessonData);
      foreach ($lesson_item['activities'] as $activity_delta => $lesson_activity_item) {
        $activity_item = $this->getItemByUuid($lesson_activity_item['target_uuid'], $this->activityData);
        // @see Drupal\lms\Controller\CourseController::activityFormTitle().
        $assert_session->pageTextContains($lesson_item['values']['name'] . ' - ' . $activity_item['values']['name']);

        // Backwards navigation button check.
        if ($lesson_delta === 0 && $activity_delta === 0) {
          $this::assertFalse($page->hasButton('edit-back'), "Backwards navigation shouldn't be possible on the first activity.");
        }
        elseif ($lesson_item['values']['backwards_navigation']) {
          $this::assertTrue($page->hasButton('edit-back'), 'Backwards navigation should be enabled.');
        }
        else {
          $this::assertFalse($page->hasButton('edit-back'), 'Backwards navigation should be disabled.');
        }

        $this->answerActivity($activity_item, (int) $lesson_activity_item['max_score'], $this->users['student']->id(), $course_id);
      }
    }

    $assert_session->pageTextContains('Course completed, please wait for evaluation.');
  }

  /**
   * Course evaluation and results test.
   */
  private function course1EvaluationTest(): void {
    $page = $this->getSession()->getPage();

    $this->drupalLogin($this->users['teacher']);

    // Tested course.
    $course_item = $this->courseData[1];

    $course_id = $this->getEntityIdByProperties('group', ['label' => $course_item['values']['label']]);
    $student_id = $this->users['student']->id();

    // Results page.
    $this->drupalGet(Url::fromRoute('lms.group.results', [
      'group' => $course_id,
      'user' => $student_id,
    ]));

    // General status.
    $status_element = $page->find('css', '[data-lms-selector="course-status"]');
    $this::assertEquals($status_element->getText(), CourseStatus::getStatusText(CourseStatus::STATUS_NEEDS_EVALUATION));

    $lesson_wrappers = $page->findAll('css', '.lesson-score-details');
    $this::assertEquals(\count($lesson_wrappers), \count($course_item['lessons']), 'The number of lessons in results should match the number of lessons in the course.');
    foreach ($lesson_wrappers as $wrapper) {
      $wrapper->click();
    }

    foreach ($course_item['lessons'] as $lesson_delta => $course_lesson_item) {
      $lesson_item = $this->getItemByUuid($course_lesson_item['target_uuid'], $this->lessonData);

      foreach ($lesson_item['activities'] as $lesson_activity_item) {
        $activity_item = $this->getItemByUuid($lesson_activity_item['target_uuid'], $this->activityData);

        $this->assertElementTextContains($lesson_wrappers[$lesson_delta], $activity_item['values']['name'], \sprintf('Activity %s not found in lesson results.', $activity_item['values']['name']));

        $answer_data = $this->getAnswerData($activity_item['uuid'], $student_id, $course_id);
        if ($answer_data['evaluated'] === FALSE) {
          $this->evaluateAnswerModal($activity_item, $lesson_item, $student_id, $course_id);
        }
      }
    }

    $result = $this->calculateCourseResult($course_item, $student_id);
    if ($result[0]) {
      $status = CourseStatus::STATUS_PASSED;
    }
    else {
      $status = CourseStatus::STATUS_FAILED;
    }
    $expected_status = CourseStatus::getStatusText($status) . ' (' . $result[1] . '%)';
    $status_element = $page->find('css', '[data-lms-selector="course-status"]');
    $answer_scores = [];
    foreach ($this->answers[$course_id][$student_id] as $answer) {
      $answer_scores[] = $answer['score'];
    }
    $this::assertEquals($status_element->getText(), $expected_status, 'Unexpected status visible on page. Answer data:' . \PHP_EOL . Yaml::encode($this->answers[$course_id][$student_id]));
  }

  /**
   * Course 2 test.
   */
  private function course2StudentTest(): void {
    $page = $this->getSession()->getPage();
    $assert_session = $this->assertSession();

    $this->drupalLogin($this->users['student']);

    // Test additional course options on the second test course.
    $test_course_data = $this->courseData[2];
    $course_id = $this->getEntityIdByProperties('group', ['label' => $test_course_data['values']['label']]);
    $this->drupalGet(Url::fromRoute('entity.group.canonical', [
      'group' => $course_id,
    ]));
    $page->clickLink('Enroll');
    $page->pressButton('edit-submit');
    $page->clickLink('Start');

    // Try to go to an arbitrary activity.
    $arbitrary_activity_route_parameters = [
      'group' => $course_id,
      'lesson_delta' => 1,
      'activity_delta' => 2,
    ];
    $this->drupalGet(Url::fromRoute('lms.group.answer_form', $arbitrary_activity_route_parameters));
    $assert_session->pageTextContains('This course does not allow free navigation.');

    $activity_count = 0;
    $back_nav_count = 0;
    $first_title = '';
    $arbitrary_activity_title = '';
    foreach ($test_course_data['lessons'] as $lesson_delta => $course_lesson_item) {
      $lesson_item = $this->getItemByUuid($course_lesson_item['target_uuid'], $this->lessonData);
      foreach ($lesson_item['activities'] as $activity_delta => $lesson_activity_item) {
        $activity_item = $this->getItemByUuid($lesson_activity_item['target_uuid'], $this->activityData);
        // @see Drupal\lms\Controller\CourseController::activityFormTitle().
        $title = $lesson_item['values']['name'] . ' - ' . $activity_item['values']['name'];
        $assert_session->pageTextContains($title);

        // Assign values needed for later asserts.
        if ($first_title === '') {
          $first_title = $title;
        }
        if (
          $lesson_delta === $arbitrary_activity_route_parameters['lesson_delta'] &&
          $activity_delta === $arbitrary_activity_route_parameters['activity_delta']
        ) {
          $arbitrary_activity_title = $title;
        }

        // Different backwards navigation button check compared to previous
        // test method - go back if allowed and resubmit.
        $back_button = $page->findButton('edit-back');
        if ($back_button !== NULL) {
          $back_button->press();
          $back_nav_count++;
          $page->pressButton('edit-submit');
        }

        $this->answerActivity($activity_item, (int) $lesson_activity_item['max_score'], $this->users['student']->id(), $course_id);
        $activity_count++;
      }

    }

    // At least one lesson must allow backwards navigation.
    $this::assertTrue($back_nav_count > 0, "Back navigation wasn't ever used.");

    // Evaluate course programmatically as that's already tested.
    $this->evaluateCourse($course_id, $this->users['student']->id());

    $this->drupalGet(Url::fromRoute('entity.group.canonical', [
      'group' => $course_id,
    ]));
    $page->clickLink('Revisit');

    // Travel all the way back to the first activity.
    for ($i = $activity_count; $i > 1; $i--) {
      $page->pressButton('edit-back');
    }
    $assert_session->pageTextContains($first_title);

    // Now arbitrary activity should be accessible.
    $this->drupalGet(Url::fromRoute('lms.group.answer_form', $arbitrary_activity_route_parameters));
    $assert_session->pageTextContains($arbitrary_activity_title);
  }

  /**
   * Exam test.
   */
  private function examTest(): void {
    $page = $this->getSession()->getPage();
    $assert_session = $this->assertSession();

    $this->drupalLogin($this->users['student']);

    // Test additional course options on the second test course.
    $test_course_data = $this->courseData[3];
    $course_id = $this->getEntityIdByProperties('group', ['label' => $test_course_data['values']['label']]);
    $this->drupalGet(Url::fromRoute('entity.group.canonical', [
      'group' => $course_id,
    ]));
    $page->clickLink('Enroll');
    $page->pressButton('edit-submit');
    $start_time = \time();
    $page->clickLink('Start');

    \sleep(1);
    $timer_element = $page->find('css', '[data-lms-close-time]');
    $close_time = (int) $timer_element->getAttribute('data-lms-close-time');
    $time_limit = $test_course_data['lessons'][0]['time_limit'] * 60;
    $expected_lock_time = $start_time + $time_limit;

    $difference = \abs($close_time - $expected_lock_time);
    // Allow a 2 second difference to account for a longer request etc.
    self::assertLessThan(2, $difference, 'Lesson close time is different than expected.');

    $lesson_item = $this->getItemByUuid($test_course_data['lessons'][0]['target_uuid'], $this->lessonData);

    // Answer all activities except the last one.
    for ($i = 0; $i < \count($lesson_item['activities']) - 1; $i++) {
      $lesson_activity_item = $lesson_item['activities'][$i];
      $activity_item = $this->getItemByUuid($lesson_activity_item['target_uuid'], $this->activityData);
      $this->answerActivity($activity_item, (int) $lesson_activity_item['max_score'], $this->users['student']->id(), $course_id);
    }

    // Wait until the time is finished.
    // @todo Evil but is there a different solution really?
    \sleep($time_limit);
    $assert_session->pageTextContains('Lesson is over time, course finished.');

    // Evaluate course programmatically so further tests are not affected.
    $this->evaluateCourse($course_id, $this->users['student']->id());
  }

  /**
   * LMS Reference table widget test - complex nesting scenario.
   */
  private function lmsReferenceTableTest(): void {
    // Some data that will be verified.
    $course_name = 'Table Widget Test Course';
    $lesson_names = [
      $this->lessonData[2]['values']['name'],
      'Table Widget Test Lesson',
    ];
    $activity_names = [
      'Table Widget Test Activity 1',
      'Table Widget Test Activity 2',
    ];
    $required_score_value = '80';

    // Test as a teacher user to properly check access.
    $this->drupalLogin($this->users['teacher']);

    // ## Creation test.
    $this->drupalGet(Url::fromRoute('entity.group.add_form', [
      'group_type' => 'lms_course',
    ]));
    $this->setEntityFormField('label', $course_name);

    $page = $this->getSession()->getPage();
    $assert_session = $this->assertSession();
    \assert($assert_session instanceof JSWebAssert);

    // Add an existing lesson.
    $page->pressButton('Reference lessons');
    $modal_selector = '[role="dialog"].modal-view-lessons-selection';
    $modal = $assert_session->waitForElementVisible('css', $modal_selector);
    $lesson = $this->getEntityByProperties('lms_lesson', [
      'name' => $lesson_names[0],
    ]);
    $selector = \sprintf('[value="%s"]', \implode(':', [
      $lesson->getEntityTypeId(),
      $lesson->bundle(),
      $lesson->id(),
    ]));
    $modal->find('css', $selector)->check();

    $modal->find('css', '.ui-dialog-buttonset > .lms-add-references')->click();
    $assert_session->waitForElementRemoved('css', $modal_selector);

    // Create a new lesson.
    $page->pressButton('Create lesson');
    $lesson_modal_selector = '[role="dialog"].modal-entity-lms-lesson';
    $lesson_modal = $assert_session->waitForElementVisible('css', $lesson_modal_selector);
    // Create 2 activities in a nested modal.
    $activity_ids = [];
    foreach ($activity_names as $activity_name) {
      $lesson_modal->find('named', ['button', 'Create activity'])->click();

      $activity_modal_selector = '[role="dialog"].modal-entity-lms-activity';
      $activity_modal = $assert_session->waitForElementVisible('css', $activity_modal_selector);
      $activity_modal->find('css', '[name="bundle"]')->selectOption('no_answer');
      $activity_form = $assert_session->waitForElementVisible('css', 'form.lms-activity-form');
      $activity_form->find('css', '[name="name[0][value]"]')->setValue($activity_name);
      $activity_modal->find('css', '.ui-dialog-buttonset > button')->click();
      $assert_session->waitForElementRemoved('css', $activity_modal_selector);

      // Check if activity has been created.
      $activity = $this->getEntityByProperties('lms_activity', [
        'name' => $activity_name,
      ]);
      $activity_ids[] = $activity->id();
    }

    // Save the lesson.
    $lesson_modal->find('css', '.ui-dialog-buttonset > button')->click();
    // Whoops, we forgot to set the name (validation test).
    $messages = $assert_session->waitForElementVisible('css', $lesson_modal_selector . ' [data-drupal-messages]');
    self::assertTrue(\strpos($messages->getText(), 'Name field is required') !== FALSE);
    // Correct and save again.
    $lesson_modal->find('css', '[name="name[0][value]"]')->setValue($lesson_names[1]);
    $lesson_modal->find('css', '.ui-dialog-buttonset > button')->click();
    $assert_session->waitForElementRemoved('css', $lesson_modal_selector);

    // Set parameters for the newly created lesson and verify the lesson
    // got created at the same time.
    $lesson = $this->getEntityByProperties('lms_lesson', [
      'name' => $lesson_names[1],
    ]);
    foreach ($lesson->get('activities') as $delta => $item) {
      self::assertEquals($item->get('target_id')->getValue(), $activity_ids[$delta]);
    }
    $selector = \sprintf('[data-drupal-selector="edit-lessons-table-%d-actions-edit-parameters"]', $lesson->id());
    $page->find('css', $selector)->click();
    $parameters_modal_selector = '[role="dialog"].modal-entity-lms-lesson';
    $parameters_modal = $assert_session->waitForElementVisible('css', $parameters_modal_selector);
    $parameters_modal->find('css', '[name="required_score"]')->setValue('80');
    $parameters_modal->find('css', '.ui-dialog-buttonset > button')->click();
    $assert_session->waitForElementRemoved('css', $parameters_modal_selector);

    // Save course, verify data.
    $page->find('css', '[data-drupal-selector="group-lms-course-add-form"] #edit-submit')->click();
    $assert_session->pageTextContains(\sprintf('Course %s has been created', $course_name));
    $course = $this->getEntityByProperties('group', [
      'label' => $course_name,
    ]);
    $lesson_items = $course->get('lessons');
    self::assertEquals(\count($lesson_names), $lesson_items->count());
    // Check if our modified max score got saved.
    /** @var \Drupal\lms\Plugin\Field\FieldType\LMSReferenceItem */
    $item = $lesson_items->get(1);
    self::assertEquals($item->getRequiredScore(), (int) $required_score_value);

    // ## Edition test.
    $this->drupalGet(Url::fromRoute('entity.group.edit_form', [
      'group' => $course->id(),
    ]));
    $selector = \sprintf('[data-drupal-selector="edit-lessons-table-%d-actions-edit-entity"]', $lesson->id());
    $page->find('css', $selector)->click();
    $lesson_modal_selector = '[role="dialog"].modal-entity-lms-lesson';
    $lesson_modal = $assert_session->waitForElementVisible('css', $lesson_modal_selector);

    // Rename lesson.
    $last_lesson_key = \count($lesson_names) - 1;
    $lesson_names[$last_lesson_key] = $lesson->label() . ' Renamed';
    $lesson_modal->find('css', '[name="name[0][value]"]')->setValue($lesson_names[$last_lesson_key]);

    // Rename the last of the lesson activities.
    // @phpstan-ignore variable.undefined
    $selector = \sprintf('[data-drupal-selector="edit-activities-table-%d-actions-edit-entity"]', $activity->id());
    $page->find('css', $selector)->click();
    $activity_modal_selector = '[role="dialog"].modal-entity-lms-activity';
    $activity_modal = $assert_session->waitForElementVisible('css', $activity_modal_selector);
    // No bundle selection this time, the modal should contain the name field.
    $last_activity_key = \count($activity_names) - 1;
    // @phpstan-ignore variable.undefined
    $activity_names[$last_activity_key] = $activity->label() . ' Renamed';
    $activity_modal->find('css', '[name="name[0][value]"]')->setValue($activity_names[$last_activity_key]);
    $activity_modal->find('css', '.ui-dialog-buttonset > button')->click();
    $assert_session->waitForElementRemoved('css', $activity_modal_selector);
    self::assertGreaterThan(0, \strpos($lesson_modal->getText(), $activity_names[$last_activity_key]), 'Changed activity title not visible on the lesson modal');

    // Try referencing one more activity.
    $activity_names[] = $this->activityData[2]['values']['name'];
    $lesson_modal->find('css', '[data-drupal-selector="edit-activities-reference-item"]')->click();
    $modal_selector = '[role="dialog"].modal-view-activities-selection';
    $modal = $assert_session->waitForElementVisible('css', $modal_selector);
    $activity = $this->getEntityByProperties('lms_activity', [
      'name' => \end($activity_names),
    ]);
    $selector = \sprintf('[value="%s"]', \implode(':', [
      $activity->getEntityTypeId(),
      $activity->bundle(),
      $activity->id(),
    ]));
    $modal->find('css', $selector)->check();
    $modal->find('css', '.ui-dialog-buttonset > .lms-add-references')->click();
    $assert_session->waitForElementRemoved('css', $modal_selector);
    self::assertGreaterThan(0, \strpos($lesson_modal->getText(), $activity->label()), 'Added activity title not visible on the lesson modal');

    // Save lesson, verify data.
    // NOTE: verify data by visiting pages rather than loading entities
    // as the test runner is running in a single request so static cache
    // will show edited entities state from before the changes were made
    // here.
    $lesson_modal->find('css', '.ui-dialog-buttonset > button')->click();
    $assert_session->waitForElementRemoved('css', $lesson_modal_selector);
    $assert_session->pageTextContains($lesson_names[$last_lesson_key]);
    // There were no changes to the course itself but let's save it to verify
    // there are no errors.
    $page->find('css', '[data-drupal-selector="group-lms-course-edit-form"] #edit-submit')->click();
    $assert_session->pageTextContains(\sprintf('Course %s has been updated', $course_name));

    $lesson_id = $this->getEntityIdByProperties('lms_lesson', [
      'name' => $lesson_names[$last_lesson_key],
    ]);
    $this->drupalGet(Url::fromRoute('entity.lms_lesson.edit_form', [
      'lms_lesson' => $lesson_id,
    ]));
    $table_text = $page->find('css', '[data-lms-selector="lms-reference-table-lms-activity"]')->getText();
    foreach ($activity_names as $delta => $activity_name) {
      $position = 0;
      $title_position = \strpos($table_text, $activity_name);
      self::assertGreaterThan($position, $title_position, 'Expected activity not referenced by the test lesson or out of order.');
      $position = $title_position;
    }

    // Removing lesson activity.
    $activity_remove_selector = '[name="remove-lms_activity-' . $activity->id() . '"]';
    $page->find('css', $activity_remove_selector)->click();
    $page->find('css', '[data-drupal-selector="edit-submit"]')->click();
    $this->screenShot();
    $assert_session->pageTextContains(\sprintf('Lesson %s has been updated', $lesson_names[$last_lesson_key]));
    $this->drupalGet(Url::fromRoute('entity.lms_lesson.edit_form', [
      'lms_lesson' => $lesson_id,
    ]));
    $assert_session->pageTextNotContains($activity->label());
    self::assertNull($page->find('css', $activity_remove_selector));
  }

  /**
   * Data integrity checks test.
   */
  private function testDataIntegrityChecks(): void {
    $page = $this->getSession()->getPage();

    $this->drupalLogin($this->users['admin']);

    // We are not supposed to be able to delete a used activity or lesson.
    $this->drupalGet(Url::fromRoute('entity.lms_lesson.delete_form', [
      'lms_lesson' => $this->getEntityIdByProperties('lms_lesson', [
        'name' => $this->lessonData[1]['values']['name'],
      ]),
    ]));
    $submit = $page->find('css', '[data-drupal-selector="edit-submit"]');
    self::assertEquals('disabled', $submit->getAttribute('disabled'));
    $this->drupalGet(Url::fromRoute('entity.lms_activity.delete_form', [
      'lms_activity' => $this->getEntityIdByProperties('lms_activity', [
        'name' => $this->activityData[1]['values']['name'],
      ]),
    ]));
    $submit = $page->find('css', '[data-drupal-selector="edit-submit"]');
    self::assertEquals('disabled', $submit->getAttribute('disabled'));

    // Unused activity - no disabled attribute.
    $this->drupalGet(Url::fromRoute('entity.lms_activity.delete_form', [
      'lms_activity' => $this->getEntityIdByProperties('lms_activity', [
        'name' => $this->activityData[8]['values']['name'],
      ]),
    ]));
    $submit = $page->find('css', '[data-drupal-selector="edit-submit"]');
    self::assertEquals(NULL, $submit->getAttribute('disabled'));

    // All courses are currently finished and evaluated - no warnings.
    $course_id = $this->getEntityIdByProperties('group', ['label' => $this->courseData[1]['values']['label']]);
    $lesson_id = $this->getEntityIdByProperties('lms_lesson', ['name' => $this->lessonData[1]['values']['name']]);
    $this->drupalGet(Url::fromRoute('entity.group.edit_form', [
      'group' => $course_id,
    ]));
    $warnings = $page->findAll('css', '.messages--warning');
    self::assertEquals(0, \count($warnings), 'No warnings expected');
    $this->drupalGet(Url::fromRoute('entity.lms_lesson.edit_form', [
      'lms_lesson' => $lesson_id,
    ]));
    $warnings = $page->findAll('css', '.messages--warning');
    self::assertEquals(0, \count($warnings), 'No warnings expected');

    // Warnings on started course and lesson pages.
    $this->drupalGet(Url::fromRoute('entity.group.canonical', [
      'group' => $course_id,
    ]));
    $page->clickLink('Enroll');
    $page->pressButton('edit-submit');
    $page->clickLink('Start');
    $this->drupalGet(Url::fromRoute('entity.group.edit_form', [
      'group' => $course_id,
    ]));
    $warnings = $page->findAll('css', '.messages--warning');
    self::assertGreaterThan(0, \count($warnings), 'Warnings expected');
    $this->drupalGet(Url::fromRoute('entity.lms_lesson.edit_form', [
      'lms_lesson' => $lesson_id,
    ]));
    $warnings = $page->findAll('css', '.messages--warning');
    self::assertGreaterThan(0, \count($warnings), 'Warnings expected');
    // Second lesson wasn't started yet, no warnings expected.
    $this->drupalGet(Url::fromRoute('entity.lms_lesson.edit_form', [
      'lms_lesson' => $this->getEntityIdByProperties('lms_lesson', [
        'name' => $this->lessonData[2]['values']['name'],
      ]),
    ]));
    $warnings = $page->findAll('css', '.messages--warning');
    self::assertEquals(0, \count($warnings), 'No warnings expected');
  }

}
