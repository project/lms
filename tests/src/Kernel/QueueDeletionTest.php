<?php

declare(strict_types=1);

namespace Drupal\Tests\lms\Kernel;

use Drupal\KernelTests\KernelTestBase;
use Drupal\Tests\user\Traits\UserCreationTrait;
use Drupal\group\Entity\Group;
use Drupal\lms\Entity\Answer;
use Drupal\lms\Entity\CourseStatus;
use Drupal\lms\Entity\LessonStatus;

/**
 * Tests lesson status deletion.
 *
 * @group lms
 */
final class QueueDeletionTest extends KernelTestBase {

  use UserCreationTrait;

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'system',
    'field',
    'flexible_permissions',
    'group',
    'lms',
    'options',
    'text',
    'user',
    'views',
  ];

  /**
   * Testing lesson statuses.
   *
   * @var \Drupal\lms\Entity\LessonStatus[]
   */
  protected array $lessonStatus = [];

  /**
   * Testing answers.
   *
   * @var \Drupal\lms\Entity\Answer[][]
   */
  protected array $answer = [];

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();

    $this->installEntitySchema('action');
    $this->installConfig(['group']);
    $this->installConfig(['lms']);
    $this->installEntitySchema('user');
    $this->installEntitySchema('view');
    $this->installEntitySchema('group');
    $this->installEntitySchema('group_relationship');
    $this->installEntitySchema('group_config_wrapper');
    $this->installEntitySchema('lms_course_status');
    $this->installEntitySchema('lms_lesson_status');
    $this->installEntitySchema('lms_answer');
  }

  /**
   * @covers \lms_lms_course_status_delete
   * @covers \Drupal\lms\Plugin\QueueWorker\DeleteEntitiesWorker
   */
  public function testDeletion(): void {
    $courseStatus = CourseStatus::create([
      'gid' => Group::create([
        'type' => 'lms_course',
        'name' => 'Test course',
        'uid' => $this->createUser(),
      ])->save(),
      'uid' => $this->createUser(),
    ]);
    $courseStatus->save();

    // Create lesson statuses and answers based on the course status.
    $this->createContent($courseStatus);

    // Delete the course status.
    $courseStatus->delete();

    // Asserts content still exists before running the queue.
    $this->assertContentExists();

    // Run cron which also runs the queue.
    $this->container->get('cron')->run();

    // Assert related lesson statuses and answers were deleted.
    $this->assertContentDeleted();
  }

  /**
   * Creates testing lesson statuses ans answers based on a give course status.
   *
   * @param \Drupal\lms\Entity\CourseStatus $courseStatus
   *   The course status.
   */
  private function createContent(CourseStatus $courseStatus): void {
    for ($i = 0; $i < 10; $i++) {
      $this->lessonStatus[$i] = LessonStatus::create([
        'lms_course' => $courseStatus->get('gid')->target_id,
        'course_status' => $courseStatus,
      ]);
      $this->lessonStatus[$i]->save();

      for ($j = 0; $j < 10; $j++) {
        $this->answer[$i][$j] = Answer::create([
          'lesson_status' => $this->lessonStatus[$i]->id(),
        ]);
        $this->answer[$i][$j]->save();
      }
    }
  }

  /**
   * Asserts that testing lesson statuses and answers exist.
   */
  private function assertContentExists(): void {
    for ($i = 0; $i < 10; $i++) {
      $reloaded = LessonStatus::load($this->lessonStatus[$i]->id());
      $this::assertInstanceOf(LessonStatus::class, $reloaded);
      $this::assertSame($this->lessonStatus[$i]->id(), $reloaded->id());
      for ($j = 0; $j < 10; $j++) {
        $reloaded = Answer::load($this->answer[$i][$j]->id());
        $this::assertInstanceOf(Answer::class, $reloaded);
        $this::assertSame($this->answer[$i][$j]->id(), $reloaded->id());
      }
    }
  }

  /**
   * Asserts that testing lesson statuses and answers are deleted.
   */
  private function assertContentDeleted(): void {
    for ($i = 0; $i < 10; $i++) {
      $this::assertNull(LessonStatus::load($this->lessonStatus[$i]->id()));
      for ($j = 0; $j < 10; $j++) {
        $this::assertNull(Answer::load($this->answer[$i][$j]->id()));
      }
    }
  }

}
